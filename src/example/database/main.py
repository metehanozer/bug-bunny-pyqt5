from PyQt5 import QtWidgets, QtSql

from connection import createConnection
from views import PersonWidget, ItemsWidget


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.mdiarea = QtWidgets.QMdiArea()
        self.setCentralWidget(self.mdiarea)

        sub1 = QtWidgets.QMdiSubWindow()
        sub1.setWidget(PersonWidget())
        self.mdiarea.addSubWindow(sub1)
        sub1.show()

        sub2 = QtWidgets.QMdiSubWindow()
        sub2.setWidget(ItemsWidget())
        self.mdiarea.addSubWindow(sub2)
        sub2.show()


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)

    if not createConnection():
        sys.exit(-1)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
