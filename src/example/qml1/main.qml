import QtQuick 2.9
import QtQuick.Controls 2.4

ApplicationWindow {
    width: 600
    visible: true
    height: 400
    title: qsTr("Consulter (Consulter les personnes enregistrer)")
    GridView {
        id: gridView
        anchors.fill: parent
        keyNavigationWraps: true
        cellWidth: 220
        cellHeight: 320
        visible: true
        model: manager.model // QML connection to python model
        delegate: Rectangle {
            id: thumb_frame
            height: 330
            width: 200
            Row{
                Text{
                    id: contactnumero
                    text: numero //
                }
                Text{
                    id: contactnom
                    text: nom
                }
            }
        }
    }
}