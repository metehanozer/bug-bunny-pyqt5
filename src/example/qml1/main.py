from PyQt5 import QtCore, QtGui, QtSql, QtQml


class SqlQueryModel(QtSql.QSqlQueryModel):
    def data(self, index, role=QtCore.Qt.DisplayRole):
        value = QtCore.QVariant()
        if index.isValid():
            if role < QtCore.Qt.UserRole:
                value = super(SqlQueryModel, self).data(index, role)
            else:
                columnIdx = role - QtCore.Qt.UserRole - 1;
                modelIndex = self.index(index.row(), columnIdx)
                value =super(SqlQueryModel, self).data(modelIndex, QtCore.Qt.DisplayRole)
        return value

    def roleNames(self):
        roles = dict()
        for i in range(self.record().count()):
            roles[QtCore.Qt.UserRole + i +1] = self.record().fieldName(i).encode()
        return roles


class Manager(QtCore.QObject):
    def __init__(self, parent=None):
        super(Manager, self).__init__(parent)
        self._model = SqlQueryModel(self)
        self.take_from_mysql()

    @QtCore.pyqtProperty(SqlQueryModel)
    def model(self):
        return self._model

    @QtCore.pyqtSlot()
    def take_from_mysql(self):
        self._model.setQuery("SELECT * FROM contact")

    @QtCore.pyqtSlot(str)
    def search_by_name(self, name):
        query = QtSql.QSqlQuery()
        query.prepare('''SELECT * FROM contact WHERE nom LIKE ? ORDER BY nom;''')
        query.addBindValue("%{}%".format(name))
        query.exec()
        self._model.setQuery(query)


def createConnection():
    db = QtSql.QSqlDatabase.addDatabase('QMYSQL')
    db.setHostName("localhost")
    db.setDatabaseName("agenda")
    db.setUserName("root")
    db.setPassword("")
    if not db.open():
        print('''Unable to establish a database connection.\n
            This example needs SQLite support. Please read
            the Qt SQL driver documentation for information
            how to build it.\n\n Click Cancel to exit.''')
        return False
    return True


if __name__ == '__main__':
    import sys
    sys.argv += ['--style', 'material']
    app = QtGui.QGuiApplication(sys.argv)
    if not createConnection():
        sys.exit(-1)
    manager = Manager()
    engine = QtQml.QQmlApplicationEngine()
    ctx = engine.rootContext()
    ctx.setContextProperty("manager", manager)
    engine.load("main.qml")
    sys.exit(app.exec_())
