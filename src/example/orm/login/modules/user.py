import sqlalchemy
from sqlalchemy import exists
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    user_id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String(35), nullable=False)
    password = sqlalchemy.Column(sqlalchemy.String(35), nullable=False)
    email = sqlalchemy.Column(sqlalchemy.String(35))
    contact = sqlalchemy.Column(sqlalchemy.String(50))

    def __repr__(self):
        return "<User(name='%s',password='%s',contact='%s', email='%s')>" % (
        self.name, self.password, self.contact, self.email)


engine = sqlalchemy.create_engine("sqlite:///user.db", echo='debug')
Base.metadata.create_all(engine)
DBsession = sqlalchemy.orm.sessionmaker(bind=engine)
session = DBsession()


def search(self, username, password):
    row = session.query(User).filter(User.name == username, User.password == password).first()
    if not row:
        return False
    else:
        return True