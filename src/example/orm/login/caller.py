from source.example.orm.login.modules.main import *
from source.example.orm.login.modules.login import Ui_Dialog as Ui_login
from source.example.orm.login.modules.signup import Ui_Dialog as Ui_signup
from source.example.orm.login.modules.user import User, session

from PyQt5.QtWidgets import *
import sys


# user_data = User('user.db')
class Signup(QDialog, Ui_signup):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.signupbutton.clicked.connect(self.addUser)

    def addUser(self):
        name = str(self.name.text())
        password = str(self.password.text())
        email = str(self.email.text())
        contact = str(self.contact.text())
        session.add(User(name=name, email=email, contact=contact, password=password))
        session.commit()


class Login(QDialog, Ui_login):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.loginbutton.clicked.connect(self.checkval)
        self.signupbutton.clicked.connect(self.generatesignup)

    def checkval(self):
        username= str(self.user.text())
        password=str(self.passs.text())
        row = session.query(User).filter(User.name==username, User.password==password).first()
        if not row:
            QtGui.QMessageBox.warning(
                self, 'Error', 'Wrong username or password')
        else:
            QtGui.QMessageBox.information(
                self, 'Success','Logged in'
            )
            self.accept()

    def generatesignup(self):
        signupobj = Signup()
        signupobj.exec_()


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.checklogin()

    def checklogin(self):
        loginobj = Login()
        loginobj.exec_()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()
    w = MainWindow()
    w.resize(width, height)
    w.show()
    sys.exit(app.exec_())