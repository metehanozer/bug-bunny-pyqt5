#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base

from PyQt5 import QtWidgets, QtCore, QtGui, uic

# My base structure

base = declarative_base()


class User(base):
    __tablename__ = "users"
    id_ = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String(20))

    def __repr__(self):
        return "<User(id='%s', name='%s')>" % (self.id_, self.name)


# Creating my base and my session
engine = sqlalchemy.create_engine("sqlite:///my_db.db", echo='debug')
base.metadata.create_all(engine)

DBsession = sqlalchemy.orm.sessionmaker(bind=engine)
session = DBsession()


class UserListModel(QtCore.QAbstractListModel):

    def __init__(self, session, parent=None):
        QtCore.QAbstractListModel.__init__(self, parent)
        self.session = session
        self.refresh()

    def refresh(self):
        self.users = self.session.query(User).all()

    def rowCount(self, parent):
        return len(self.users)

    def data(self, index, role):
        if role == QtCore.Qt.DisplayRole:
            value = self.users[index.row()]
            return "%s : %s" % (value.id_, value.name)


def add_user(session, tmp_name):
    session.add(User(name=tmp_name))
    session.commit()

    
def buton_click():
    session.add(User(name="Metehan3"))
    session.commit()


# Populating my db
if not session.query(User).count():
    session.add(User(name="edouard"))
    session.add(User(name="jean"))
    session.add(User(name="albert"))
    session.commit()

# Creating my app
app = QtWidgets.QApplication(sys.argv)
mywindow = QtWidgets.QWidget()

# Combobox and his model
combobox = QtWidgets.QComboBox()
combobox.setModel(UserListModel(session))

loginButton = QtWidgets.QPushButton("Login")
loginButton.clicked.connect(buton_click)

layout = QtWidgets.QHBoxLayout(mywindow)
layout.addWidget(combobox)
layout.addWidget(loginButton)

mywindow.show()
sys.exit(app.exec_())
