# -*- coding: utf-8 -*-

import sys

from PyQt5.QtCore import QAbstractItemModel, Qt, QModelIndex, QSortFilterProxyModel
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtWidgets import (QApplication, QMainWindow, QVBoxLayout, QLineEdit, QTreeView, QHBoxLayout, QGroupBox,
                             QWidget)

from source.example.mv_pattern.mv_p1 import icons_rc

sys._excepthook = sys.excepthook


def my_exception_hook(exctype, value, traceback):
    sys._excepthook(exctype, value, traceback)
    sys.exit(1)


class Node(object):

    def __init__(self, name, parent=None):
        self._name = name
        self._parent = parent
        self._children = []

        if parent is not None:
            parent.addChild(self)

    def typeInfo(self):
        return "NODE"

    def addChild(self, child):
        self._children.append(child)

    def insertChild(self, position, child):
        if position < 0 or position > len(self._children):
            return False

        self._children.insert(position, child)
        child._parent = self
        return True

    def removeChild(self, position):
        if position < 0 or position > len(self._children):
            return False

        child = self._children.pop(position)
        child._parent = None
        return True

    def name(self):
        return self._name

    def setName(self, name):
        self._name = name

    def child(self, row):
        return self._children[row]

    def childCount(self):
        return len(self._children)

    def parent(self):
        return self._parent

    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)

    def log(self, tabLevel=-1):
        output = ""
        tabLevel += 1

        for i in range(tabLevel):
            output += "\t"

        output += "|-----" + self._name + "\n"

        for child in self._children:
            output += child.log(tabLevel)

        tabLevel -= 1

        # output += "\n"

        return output

    def __repr__(self):
        return self.log()


class TransformNode(Node):

    def __init__(self, name, parent=None):
        super(TransformNode, self).__init__(name, parent)

        self._x = 0
        self._y = 0
        self._z = 0

    def typeInfo(self):
        return "TRANSFORM"

    def x(self):
        return self._x

    def y(self):
        return self._y

    def z(self):
        return self._z

    def setX(self, x):
        self._x = x

    def setY(self, y):
        self._y = y
    
    def setZ(self, z):
        self._z = z


class CameraNode(Node):

    def __init__(self, name, parent=None):
        super(CameraNode, self).__init__(name, parent)

        self._motionBlur = True
        self._shakeIntensity = 50,0

    def typeInfo(self):
        return "CAMERA"

    def motionBlur(self):
        return self._motionBlur

    def shakeIntensity(self):
        return self._shakeIntensity

    def setMotionBlur(self, blur):
        self._motionBlur = blur

    def setShakeIntensity(self, intensity):
        self._shakeIntensity = intensity


class LightNode(Node):

    def __init__(self, name, parent=None):
        super(LightNode, self).__init__(name, parent)

        self._lightIntensity = 1,0
        self._nearRange = 40,0
        self._farRange = 80,0
        self._castShadows = True

    def typeInfo(self):
        return "LIGHT"

    def lightIntensity(self):
        return self._lightIntensity

    def nearRange(self):
        return self._nearRange

    def farRange(self):
        return self._farRange

    def castShadows(self):
        return self._castShadows

    def setLightIntensity(self, intensity):
        self._lightIntensity = intensity

    def setNearRange(self, range):
        self._nearRange = range

    def setFarRange(self, range):
        self._farRange = range

    def setCastShadows(self, shadows):
        self._castShadows = shadows


class MyTreeModel(QAbstractItemModel):

    sortRole = Qt.UserRole
    filterRole = Qt.UserRole + 1

    def __init__(self, root, parent=None):
        super(MyTreeModel, self).__init__(parent)
        self._root = root

    def rowCount(self, parent):
        if not parent.isValid():
            parent_node = self._root
        else:
            parent_node = parent.internalPointer()

        return parent_node.childCount()

    def columnCount(self, parent):
        # return 2
        return 1

    def data(self, index, role):

        if not index.isValid():
            return None

        node = index.internalPointer()

        if role == Qt.DisplayRole or role == Qt.EditRole:
            if index.column() == 0:
                return node.name()
            if index.column() == 1:
                return node.typeInfo()

        if role == Qt.DecorationRole:
            if index.column() == 0:
                type_info = node.typeInfo()

                if type_info == "CAMERA":
                    return QIcon(QPixmap(":/camera.png"))
                if type_info == "LIGHT":
                    return QIcon(QPixmap(":/light.png"))
                if type_info == "TRANSFORM":
                    return QIcon(QPixmap(":/transform.png"))

        if role == MyTreeModel.sortRole:
            return node.typeInfo()

        if role == MyTreeModel.filterRole:
            return node.typeInfo()


    def setData(self, index, value, role=Qt.EditRole):
        if index.isValid():
            if role == Qt.EditRole:
                node = index.internalPointer()
                node.setName(value)
                return True
        return False

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if section == 0:
                return "Header"
            else:
                return "TypeInfo"

    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable

    def parent(self, index):
        node = index.internalPointer()
        parent_node = node.parent()
        if parent_node == self._root:
            return QModelIndex()

        return self.createIndex(parent_node.row(), 0, parent_node)

    def index(self, row, column, parent):
        if not parent.isValid():
            parent_node = self._root
        else:
            parent_node = parent.internalPointer()

        child_item = parent_node.child(row)
        if child_item:
            return self.createIndex(row, column, child_item)
        else:
            return QModelIndex()

    def getNode(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node

        return self._root

    def insertRows(self, position, rows, parent=QModelIndex()):

        parent_node = self.getNode(parent)
        child_count = parent_node.childCount()

        self.beginInsertRows(parent, position, position + rows - 1)

        for row in range(rows):
            child_node = Node("Untitled" + str(child_count))
            success = parent_node.insertChild(position, child_node)

        self.endInsertRows()

        return success

    def insertLights(self, position, rows, parent=QModelIndex()):

        parent_node = self.getNode(parent)
        child_count = parent_node.childCount()

        self.beginInsertRows(parent, position, position + rows - 1)

        for row in range(rows):
            child_node = LightNode("Light" + str(child_count))
            success = parent_node.insertChild(position, child_node)

        self.endInsertRows()

        return success

    def removeRows(self, position, rows, parent=QModelIndex()):

        parent_node = self.getNode(parent)

        self.beginRemoveRows(parent, position, position + rows - 1)

        for row in range(rows):
            success = parent_node.removeChild(position)

        self.endRemoveRows()

        return success


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.line_edit = None
        self.tree_view = None
        self._data_model = None
        self._proxyModel = None

        self.centralwidget = QWidget(self)
        self.setCentralWidget(self.centralwidget)

        layout = QVBoxLayout(self.centralwidget)
        layout.addWidget(self.init_filter())
        layout.addWidget(self.init_tree())
        layout.setContentsMargins(15, 15, 15, 15)

        # self.setLayout(layout)
        self.setLayoutDirection(Qt.LeftToRight)
        self.setAutoFillBackground(False)
        self.setAcceptDrops(False)

        self.init_data()

    def init_filter(self):

        self.line_edit = QLineEdit(self)

        layout = QHBoxLayout()
        layout.addWidget(self.line_edit)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_tree(self):

        self.tree_view = QTreeView(self)

        layout = QHBoxLayout()
        layout.addWidget(self.tree_view)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_data(self):

        root_node = Node("Hips")
        child_node0 = TransformNode("A node", root_node)
        child_node1 = LightNode("B node", root_node)
        child_node2 = CameraNode("C node", root_node)
        child_node3 = TransformNode("D node", root_node)
        child_node4 = CameraNode("E node", root_node)
        child_node5 = LightNode("F node", root_node)
        child_node6 = TransformNode("G node", root_node)
        child_node7 = LightNode("H node", root_node)
        child_node8 = CameraNode("I node", root_node)

        self._data_model = MyTreeModel(root_node)
        # self._data_model.insertLights(0, 8)

        self._proxyModel = QSortFilterProxyModel()
        self._proxyModel.setSourceModel(self._data_model)
        self._proxyModel.setDynamicSortFilter(True)
        self._proxyModel.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self._proxyModel.setSortRole(MyTreeModel.sortRole)
        self._proxyModel.setFilterRole(MyTreeModel.filterRole)
        self._proxyModel.setFilterKeyColumn(0)

        self.tree_view.setModel(self._proxyModel)
        self.tree_view.setSortingEnabled(True)

        # self.line_edit.editingFinished.connect(self._proxyModel.setFilterRegExp)
        self.line_edit.textEdited.connect(self._proxyModel.setFilterRegExp)


if __name__ == '__main__':

    sys.excepthook = my_exception_hook

    app = QApplication(sys.argv)

    main_window = MainWindow()
    main_window.show()
    main_window.raise_()

    try:
        app.exec_()
        sys.exit(app)
    except:
        print("Exiting")