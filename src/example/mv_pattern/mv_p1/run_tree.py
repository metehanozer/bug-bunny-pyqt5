import sys

from PyQt5.QtCore import QAbstractItemModel, Qt, QModelIndex
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtWidgets import QApplication, QTreeView

from source.example.mv_pattern.mv_p1 import icons_rc

sys._excepthook = sys.excepthook


def my_exception_hook(exctype, value, traceback):
    # Print the error and traceback
    # print(exctype, value, traceback)
    # Call the normal Exception hook after
    sys._excepthook(exctype, value, traceback)
    sys.exit(1)


class Node(object):

    def __init__(self, name, parent=None):
        self._name = name
        self._parent = parent
        self._children = []

        if parent is not None:
            parent.addChild(self)

    def typeInfo(self):
        return "NODE"

    def addChild(self, child):
        self._children.append(child)

    def insertChild(self, position, child):
        if position < 0 or position > len(self._children):
            return False

        self._children.insert(position, child)
        child._parent = self
        return True

    def removeChild(self, position):
        if position < 0 or position > len(self._children):
            return False

        child = self._children.pop(position)
        child._parent = None
        return True

    def name(self):
        return self._name

    def setName(self, name):
        self._name = name

    def child(self, row):
        return self._children[row]

    def childCount(self):
        return len(self._children)

    def parent(self):
        return self._parent

    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)

    def log(self, tabLevel=-1):
        output = ""
        tabLevel += 1

        for i in range(tabLevel):
            output += "\t"

        output += "|-----" + self._name + "\n"

        for child in self._children:
            output += child.log(tabLevel)

        tabLevel -= 1

        # output += "\n"

        return output

    def __repr__(self):
        return self.log()


class TransformNode(Node):

    def __init__(self, name, parent=None):
        super(TransformNode, self).__init__(name, parent)

    def typeInfo(self):
        return "TRANSFORM"


class CameraNode(Node):

    def __init__(self, name, parent=None):
        super(CameraNode, self).__init__(name, parent)

    def typeInfo(self):
        return "CAMERA"


class LightNode(Node):

    def __init__(self, name, parent=None):
        super(LightNode, self).__init__(name, parent)

    def typeInfo(self):
        return "LIGHT"


class MyTreeModel(QAbstractItemModel):

    def __init__(self, root, parent=None):
        super(MyTreeModel, self).__init__(parent)
        self._root = root

    def rowCount(self, parent):
        if not parent.isValid():
            parent_node = self._root
        else:
            parent_node = parent.internalPointer()

        return parent_node.childCount()

    def columnCount(self, parent):
        # return 2
        return 1

    def data(self, index, role):
        if not index.isValid():
            return None

        node = index.internalPointer()

        if role == Qt.DisplayRole or role == Qt.EditRole:
            if index.column() == 0:
                return node.name()
            # else:  # İKİNCİ KOLON
            #     return node.typeInfo()

        if role == Qt.DecorationRole:
            if index.column() == 0:
                type_info = node.typeInfo()

                if type_info == "CAMERA":
                    return QIcon(QPixmap(":/camera.png"))
                if type_info == "LIGHT":
                    return QIcon(QPixmap(":/light.png"))
                if type_info == "TRANSFORM":
                    return QIcon(QPixmap(":/transform.png"))


    def setData(self, index, value, role=Qt.EditRole):
        if index.isValid():
            if role == Qt.EditRole:
                node = index.internalPointer()
                node.setName(value)
                return True
        return False

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if section == 0:
                return "Header"
            else:
                return "TypeInfo"

    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable

    def parent(self, index):
        node = index.internalPointer()
        parent_node = node.parent()
        if parent_node == self._root:
            return QModelIndex()

        return self.createIndex(parent_node.row(), 0, parent_node)

    def index(self, row, column, parent):
        if not parent.isValid():
            parent_node = self._root
        else:
            parent_node = parent.internalPointer()

        child_item = parent_node.child(row)
        if child_item:
            return self.createIndex(row, column, child_item)
        else:
            return QModelIndex()

    def getNode(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node

        return self._root

    def insertRows(self, position, rows, parent=QModelIndex()):

        parent_node = self.getNode(parent)
        child_count = parent_node.childCount()

        self.beginInsertRows(parent, position, position + rows - 1)

        for row in range(rows):
            child_node = Node("Untitled" + str(child_count))
            success = parent_node.insertChild(position, child_node)

        self.endInsertRows()

        return success

    def insertLights(self, position, rows, parent=QModelIndex()):

        parent_node = self.getNode(parent)
        child_count = parent_node.childCount()

        self.beginInsertRows(parent, position, position + rows - 1)

        for row in range(rows):
            child_node = LightNode("Light" + str(child_count))
            success = parent_node.insertChild(position, child_node)

        self.endInsertRows()

        return success

    def removeRows(self, position, rows, parent=QModelIndex()):

        parent_node = self.getNode(parent)

        self.beginRemoveRows(parent, position, position + rows - 1)

        for row in range(rows):
            success = parent_node.removeChild(position)

        self.endRemoveRows()

        return success


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle("plastique")

    sys.excepthook = my_exception_hook

    rootNode = Node("Hips")
    childNode0 = TransformNode("LeftPirateLeg", rootNode)
    childNode1 = Node("RightLeg", childNode0)
    childNode2 = CameraNode("RightFoot", rootNode)
    childNode3 = Node("LeftTibia", childNode2)
    childNode4 = Node("LeftFoot", childNode3)
    childNode5 = LightNode("LeftFoot_END", childNode4)

    print(rootNode)

    model = MyTreeModel(rootNode)

    treeView = QTreeView()
    treeView.show()

    treeView.setModel(model)

    LeftPirateLeg = model.index(0, 0, QModelIndex())
    model.insertLights(1, 2, LeftPirateLeg)

    # model.insertRows(0, 2)
    # model.insertLights(1, 2)
    # model.removeRows(0, 2)

    try:
        sys.exit(app.exec_())
    except:
        print("Exiting")
