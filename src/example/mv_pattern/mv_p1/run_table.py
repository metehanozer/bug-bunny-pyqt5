import sys

from PyQt5.QtCore import QAbstractTableModel, Qt, QModelIndex
from PyQt5.QtGui import QPixmap, QIcon, QColor
from PyQt5.QtWidgets import QApplication, QListView, QComboBox, QTableView

sys._excepthook = sys.excepthook


class MyTableModel(QAbstractTableModel):

    def __init__(self, colors=[[]], headers=[], parent=None):
        QAbstractTableModel.__init__(self, parent)
        self.colors = colors
        self.headers = headers

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.headers[section]
            else:
                return "Color %s" % section

    def rowCount(self, parent):
        return len(self.colors)

    def columnCount(self, parent):
        try:
            return len(self.colors[0])
        except TypeError:
            return 0

    def flags(self, index):
        return Qt.ItemIsEditable | Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def data(self, index, role):

        if role == Qt.EditRole:
            row = index.row()
            column = index.column()
            value = self.colors[row][column]
            return value.name()

        if role == Qt.ToolTipRole:
            row = index.row()
            column = index.column()
            value = self.colors[row][column]
            return "Hex code: %s" % value.name()

        if role == Qt.DecorationRole:
            row = index.row()
            column = index.column()
            value = self.colors[row][column]
            pixmap = QPixmap(26, 26)
            pixmap.fill(value)
            icon = QIcon(pixmap)
            return icon

        if role == Qt.DisplayRole:
            row = index.row()
            column = index.column()
            value = self.colors[row][column]
            return value.name()

    def setData(self, index, value, role=Qt.EditRole):

        if role == Qt.EditRole:
            row = index.row()
            column = index.column()

            color = QColor(value)
            if color.isValid():
                self.colors[row][column] = color
                self.dataChanged.emit(index, index)
                return True

        return False

    def insertRows(self, position, rows, parent=QModelIndex()):

        self.beginInsertRows(parent, position, position + rows - 1)

        for i in range(rows):
            default_values = [QColor("#000000") for i in range(self.columnCount(None))]
            self.colors.insert(position, default_values)

        self.endInsertRows()

        return True

    def removeRows(self, position, rows, parent=QModelIndex()):

        self.beginRemoveRows(parent, position, position + rows - 1)

        for i in range(rows):
            value = self.colors[position]
            self.colors.remove(value)

        self.endRemoveRows()

        return True


def my_exception_hook(exctype, value, traceback):
    # Print the error and traceback
    # print(exctype, value, traceback)
    # Call the normal Exception hook after
    sys._excepthook(exctype, value, traceback)
    sys.exit(1)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle("plastique")

    sys.excepthook = my_exception_hook

    listView = QListView()
    listView.show()

    # treeView = QTreeView()
    # treeView.show()

    combobox = QComboBox()
    combobox.show()

    tableView = QTableView()
    tableView.show()

    rowCount = 4
    columnCount = 6

    headers = ["Pallette", "Colors", "Brushes", "Omg1", "Omg2", "Omg3"]
    tbleData0 = [
        [QColor(0, 255, 0), QColor(0, 255, 0), QColor(0, 255, 0), QColor(0, 255, 0)],
        [QColor(0, 255, 0), QColor(0, 255, 0), QColor(0, 255, 0), QColor(0, 255, 0)],
        [QColor(0, 255, 0), QColor(0, 255, 0), QColor(0, 255, 0), QColor(0, 255, 0)],
        [QColor(0, 255, 0), QColor(0, 255, 0), QColor(0, 255, 0), QColor(0, 255, 0)]
    ]
    tbleData1 = [[QColor(0, 255, 0) for i in range(columnCount)] for k in range(rowCount)]

    model = MyTableModel(tbleData1, headers)

    listView.setModel(model)
    # treeView.setModel(model)
    combobox.setModel(model)
    tableView.setModel(model)

    model.insertRows(0, 2)
    #model.removeRows(0, 2)

    try:
        sys.exit(app.exec_())
    except:
        print("Exiting")
