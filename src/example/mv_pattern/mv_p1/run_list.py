import sys

from PyQt5.QtCore import QAbstractListModel, Qt, QModelIndex
from PyQt5.QtGui import QPixmap, QIcon, QColor
from PyQt5.QtWidgets import QApplication, QListView, QTreeView, QComboBox, QTableView


class MyListModel(QAbstractListModel):

    def __init__(self, colors=[], parent=None):

        QAbstractListModel.__init__(self, parent)

        self.colors = colors

    def headerData(self, section, orientation, role):

        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return "Palette"
            else:
                return "Color %s" % section

    def rowCount(self, parent):

        return len(self.colors)

    def data(self, index, role):

        if role == Qt.EditRole:
            return self.colors[index.row()].name()

        if role == Qt.ToolTipRole:
            return "Hex code: %s" % self.colors[index.row()].name()

        if role == Qt.DecorationRole:
            pixmap = QPixmap(26, 26)
            pixmap.fill(self.colors[index.row()])
            icon = QIcon(pixmap)
            return icon

        if role == Qt.DisplayRole:
            return self.colors[index.row()].name()

    def flags(self, index):

        return Qt.ItemIsEditable | Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def setData(self, index, value, role=Qt.EditRole):

        if role == Qt.EditRole:
            row = index.row()
            color = QColor(value)

            if color.isValid():
                self.colors[row] = color
                self.dataChanged.emit(index, index)
                return True

        return False

    def insertRows(self, position, rows, parent=QModelIndex()):

        self.beginInsertRows(parent, position, position + rows - 1)

        for i in range(rows):
            self.colors.insert(position, QColor("#000000"))

        self.endInsertRows()

        return True

    def removeRows(self, position, rows, parent=QModelIndex()):

        self.beginRemoveRows(parent, position, position + rows - 1)

        for i in range(rows):
            value = self.colors[position]
            self.colors.remove(value)

        self.endRemoveRows()

        return True


if __name__ == '__main__':
    app = QApplication(sys.argv)
    # app.setStyle("plastique")

    listView = QListView()
    listView.show()

    treeView = QTreeView()
    treeView.show()

    combobox = QComboBox()
    combobox.show()

    tableView = QTableView()
    tableView.show()

    red = QColor(255,0,0)
    green = QColor(0,255,0)
    blue = QColor(0,0,255)

    model = MyListModel([red, green, blue])

    listView.setModel(model)
    treeView.setModel(model)
    combobox.setModel(model)
    tableView.setModel(model)

    model.insertRows(0, 2)
    model.removeRows(0, 2)

    sys.exit(app.exec_())
