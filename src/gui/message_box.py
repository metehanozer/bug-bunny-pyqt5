# -*- coding: utf-8 -*-

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMessageBox, QSizePolicy, QTextEdit

from src.util.mt import LOGGER, ICON


class MessageBox(QMessageBox):

    def __init__(self):
        QMessageBox.__init__(self)

        LOGGER.debug('MessageBox.__init__')

        self.setWindowIcon(QIcon(ICON))
        self.setWindowTitle('WARNING')

        self.setSizeGripEnabled(True)

        typ = "Information"
        if typ == "Critical":
            self.setIcon(QMessageBox.Critical)
        elif typ == "Warning":
            self.setIcon(QMessageBox.Warning)
        elif typ == "Information":
            self.setIcon(QMessageBox.Information)

    def event(self, e):
        result = QMessageBox.event(self, e)

        self.setMinimumWidth(600)
        self.setMinimumHeight(100)
        self.setMaximumWidth(1200)
        self.setMaximumHeight(900)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        text_edit = self.findChild(QTextEdit)
        if text_edit is not None:
            text_edit.setMinimumWidth(590)
            text_edit.setMinimumHeight(220)
            text_edit.setMaximumWidth(1190)
            text_edit.setMaximumHeight(16777215)
            text_edit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        return result
