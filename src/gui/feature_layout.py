# -*- coding: utf-8 -*-

import socket
import ftplib
from threading import Thread

from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QHBoxLayout, QLabel, QPushButton

from src.util.mt import get_head_tail_from
from src.util.mt import LOGGER, FTP_PARAMS
from src.util.ftp_manager import FtpManager


class FeatureLayout(QHBoxLayout):
    def __init__( self, parent, desc, path):
        super(FeatureLayout, self).__init__(parent)

        self.desc = desc
        self.path = path
        self.file_name = None
        self.ftp_path = None

        self.name_label = None
        self.link_label = None
        self.delete_button = None

        self.init_ui()

    def init_ui(self):

        self.name_label = QLabel(self.desc)
        self.name_label.setObjectName(self.desc)
        self.name_label.setFixedSize(160, 20)

        link_bytearray = bytearray(QUrl.fromLocalFile(self.path).toEncoded()).decode()
        _, self.file_name = get_head_tail_from(self.path)
        link = "<a href={}>{}</a>".format(link_bytearray, self.file_name)
        self.link_label = QLabel()
        self.link_label.setText(link)
        self.link_label.setOpenExternalLinks(True)
        self.link_label.setObjectName(self.path)

        self.delete_button = QPushButton("delete")
        # self.delete_button.setMaximumWidth(20)
        self.delete_button.clicked.connect(self.delete_button_clicked)

        self.addWidget(self.name_label)
        self.addWidget(self.link_label)
        self.addStretch(1)
        self.addWidget(self.delete_button)
        self.setSpacing(30)

    def delete_button_clicked(self):
        self.name_label.deleteLater()
        self.link_label.deleteLater()
        self.delete_button.deleteLater()
        self.deleteLater()
