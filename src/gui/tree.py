# -*- coding: utf-8 -*-

from PyQt5.QtGui import QIcon, QDrag
from PyQt5.QtCore import pyqtSlot, Qt, QMimeData
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QTextEdit, QPushButton, QGroupBox, QLineEdit, QSplitter, QTreeWidget, QTreeWidgetItem, QSizePolicy


class MtTree(QTreeWidget):
    def __init__(self, parent = None):
        super(MtTree, self).__init__(parent)
        self.setDragEnabled(True)
        self.setAcceptDrops(True)

    def startDrag(self, dropAction):
        # create mime data object
        mime = QMimeData()
        mime.setData('application/x-item', '???')
        # start drag
        drag = QDrag(self)
        drag.setMimeData(mime)
        drag.start(Qt.CopyAction | Qt.CopyAction)

    def dragMoveEvent(self, event):
        if event.mimeData().hasFormat("application/x-item"):
            event.setDropAction(Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dragEnterEvent(self, event):
        if (event.mimeData().hasFormat('application/x-item')):
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if (event.mimeData().hasFormat('application/x-item')):
            event.acceptProposedAction()
            data = Qt.QString(event.mimeData().data("application/x-item"))
            item = QTreeWidgetItem(self)
            item.setText(0, data)
            self.addTopLevelItem(item)
        else:
            event.ignore()
