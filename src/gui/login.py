# -*- coding: utf-8 -*-

import os
#import ldap

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QDialog, QGridLayout, QLineEdit, QPushButton, QLabel

from src.util.mt import TITLE, ICON, LOGGER, LDAP_PARAMS


class Login(QDialog):
    def __init__(self, parent=None):
        super(Login, self).__init__(parent)

        self.network = None
        self.username = None
        self.password = None
        self.user_edit = None
        self.pass_edit = None
        self.login_button = None
        self.info_label = None
        self.style_file = os.path.join("resource/style", "login.qss")

        # self.local_server = NETWORK_PARAMS["local"]
        # print(self.is_local_network_available())
        # if self.is_local_network_available():
        #     print("local")
        # else:
        #     print("internet")

        self.domain = LDAP_PARAMS["domain"]
        self.server_internal = LDAP_PARAMS["server_internal"]
        self.server_external = LDAP_PARAMS["server_external"]

        self.init_ui()

    def init_ui(self):

        main_layout = QGridLayout()
        main_layout.setVerticalSpacing(5)
        main_layout.setHorizontalSpacing(20)
        # main_layout.setContentsMargins(0, 0, 0, 0)

        pixmap = QPixmap('resource\\image\\asis_login.jpg')
        logo_label = QLabel(self)
        logo_label.setPixmap(pixmap)
        self.resize(pixmap.width(), pixmap.height())
        logo_label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter | Qt.AlignHCenter)
        main_layout.addWidget(logo_label, 1, 0, 1, 2)

        self.info_label = QLabel('You can login with user ldap...')
        self.info_label.setObjectName("info_label")
        self.info_label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter | Qt.AlignHCenter)
        main_layout.addWidget(self.info_label, 2, 0, 1, 2)

        user_label = QLabel('Username')
        main_layout.addWidget(user_label, 3, 0)

        self.user_edit = QLineEdit(self)
        main_layout.addWidget(self.user_edit, 3, 1)

        pass_label = QLabel('Password')
        main_layout.addWidget(pass_label, 4, 0)

        self.pass_edit = QLineEdit(self)
        self.pass_edit.setEchoMode(QLineEdit.Password)
        main_layout.addWidget(self.pass_edit, 4, 1)

        self.login_button = QPushButton('LOGIN', self)
        self.login_button.clicked.connect(self.handle_login)
        main_layout.addWidget(self.login_button, 5, 0, 1, 2)

        # self.result_label = QLabel()
        # self.result_label.setObjectName("result_label")
        # self.result_label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter | Qt.AlignHCenter)
        # main_layout.addWidget(self.result_label, 6, 0, 1, 2)

        self.setWindowTitle(TITLE)
        self.setWindowIcon(QIcon(ICON))
        self.setLayout(main_layout)

        try:
            with open(self.style_file) as style:
                self.setStyleSheet(style.read())
        except FileNotFoundError:
            LOGGER.info("%s qss dosyası bulunamadı!" % self.style_file)

    def handle_login(self):
        pass

    def handle_login2(self):
        self.username = self.user_edit.text()
        self.password = self.pass_edit.text()

        if self.username == '' or self.password == '':
            self.info_label.setStyleSheet('QLabel {color: #F46060;}')
            self.info_label.setText("Username or Password is empty!")

        else:
            ldap_username = '%s@%s' % (self.username, self.domain)

            for server in (self.server_internal, self.server_external):
                try:
                    ldap_client = ldap.initialize(server)
                    ldap_client.set_option(ldap.OPT_REFERRALS, 0)
                    ldap_client.set_option(ldap.OPT_NETWORK_TIMEOUT, 2.0)
                    ldap_client.simple_bind_s(ldap_username, self.password)
                    ldap_client.unbind()
                    self.accept()
                    # TODO self.network ile local network mü karar verilecek buna göre diğer değişkenler belirlenecek..
                    break

                except ldap.INVALID_CREDENTIALS:
                    ldap_client.unbind()
                    LOGGER.info('Wrong username or password')
                    self.info_label.setStyleSheet('QLabel {color: #F46060;}')
                    self.info_label.setText("Wrong Username or Password!")
                    self.user_edit.setText("")
                    self.user_edit.setFocus()
                    self.pass_edit.setText("")
                    break

                except ldap.SERVER_DOWN:
                    print("ldap.SERVER_DOWN")
                    ldap_client.unbind()
                    continue
            else:
                LOGGER.info('AD server not awailable')
                self.info_label.setStyleSheet('QLabel {color: #F46060;}')
                self.info_label.setText("LDAP server error!")
                self.user_edit.setText("")
                self.user_edit.setFocus()
                self.pass_edit.setText("")
                # raise ldap.SERVER_DOWN("The database engine must be PostgtreSQL or MySQL")

    def handle_login_orj(self):
        self.username = self.user_edit.text()
        self.password = self.pass_edit.text()

        if self.username == '' or self.password == '':
            self.info_label.setStyleSheet('QLabel {color: #F46060;}')
            self.info_label.setText("Username or Password is empty!")

        else:
            ldap_username = '%s@%s' % (self.username, self.domain)

            try:
                ldap_client = ldap.initialize(self.server_internal)
                ldap_client.set_option(ldap.OPT_REFERRALS, 0)
                ldap_client.simple_bind_s(ldap_username, self.password)
                ldap_client.unbind()
                self.accept()

            except ldap.INVALID_CREDENTIALS:
                ldap_client.unbind()
                LOGGER.info('Wrong username or password')
                self.info_label.setStyleSheet('QLabel {color: #F46060;}')
                self.info_label.setText("Wrong Username or Password!")
                self.user_edit.setText("")
                self.user_edit.setFocus()
                self.pass_edit.setText("")

            except ldap.SERVER_DOWN:
                LOGGER.info('AD server not awailable')
                self.info_label.setStyleSheet('QLabel {color: #F46060;}')
                self.info_label.setText("LDAP server error!")
                self.user_edit.setText("")
                self.user_edit.setFocus()
                self.pass_edit.setText("")
