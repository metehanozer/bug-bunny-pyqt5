# -*- coding: utf-8 -*-

import sys
import psycopg2

from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSettings, QPoint, QSize
from PyQt5.QtWidgets import QMainWindow, QWidget, QVBoxLayout, QTabWidget, QStatusBar

from src.util.data import get_use_id_from, get_roles_from, get_combo_data, add_log
from src.gui.tab.dashboard_tab import DashboardTab
from src.gui.tab.search_tab import SearchTab
from src.gui.tab.users_tab import UsersTab
from src.gui.tab.add_edit_defect_tab import AddEditDefectTab
from src.util.mt import TITLE, ICON, COM, LOGGER


class MainWindow(QMainWindow):
    def __init__(self, user, parent=None):
        super(MainWindow, self).__init__(parent)

        self.settings = QSettings(COM, TITLE)

        self.centralwidget = None
        self.tabwidget = None
        self.dashboardTab = None
        self.usersTab = None
        self.searchTab = None
        self.addEditDefectTab = None

        self.statusbar = None

        self.user = user

        #######################################################################

        self.use_id = get_use_id_from(self.user)

        self.roles = get_roles_from(self.user)

        #add_log(self.use_id)

        self.combo_data = get_combo_data()

        #######################################################################

        self.setWindowTitle(TITLE)
        self.setWindowIcon(QIcon(ICON))

        #######################################################################

        # try:
        #     with open('resource/light.qss') as s:
        #         self.setStyleSheet(s.read())
        # except FileNotFoundError:
        #     print("Style dosyası bulunamadı!")
        #     pass

        #######################################################################

        # with open(REST_JSON, 'r', encoding='utf-8') as f:
        #     self.rest_json = json.load(f)

        #######################################################################

        self.startEvent()

        #######################################################################

        self.init_tab()

        #######################################################################

        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)

    def init_tab(self):

        # self.move(self.setts.value('pos', QPoint(200, 100)))
        # LOGGER.debug('%s dosyasından konum bilgisi okundu: %s' % (self.setts.fileName(), self.setts.value('pos')))

        #######################################################################

        self.centralwidget = QWidget(self)
        layout = QVBoxLayout(self.centralwidget)
        # layout.setContentsMargins(15, 15, 15, 15)
        self.tabwidget = QTabWidget(self.centralwidget)
        layout.addWidget(self.tabwidget)
        self.setCentralWidget(self.centralwidget)

        #######################################################################

        if 1 in self.roles:
            self.usersTab = UsersTab(self)
            self.tabwidget.addTab(self.usersTab, 'Users')

        #######################################################################

        self.dashboardTab = DashboardTab(self)
        self.tabwidget.addTab(self.dashboardTab, 'Dashboard')

        #######################################################################

        self.addEditDefectTab = AddEditDefectTab(self)
        self.tabwidget.addTab(self.addEditDefectTab, 'Add/Edit')

        #######################################################################

        self.searchTab = SearchTab(self)
        self.tabwidget.addTab(self.searchTab, 'Search')

        #######################################################################

        self.tabwidget.currentChanged.connect(self._tab_changed)

    def _tab_changed(self, idx):
        LOGGER.debug("Seçilen tab index: %s" % idx)

        # SEÇİLEN TABA GÖRE VERİLER YÜKLENİYOR
        if self.tabwidget.currentWidget() is self.dashboardTab:
            LOGGER.debug("dashboardTab seçildi...")

        elif self.tabwidget.currentWidget() is self.addEditDefectTab:
            LOGGER.debug("addEditDeffectTab seçildi...")

            # self.addEditDefectTab.edit_defect()

        elif self.tabwidget.currentWidget() is self.searchTab:
            LOGGER.debug("searchTab seçildi...")

    def connect_db(self):
        try:
            LOGGER.debug('Connecting to the PostgreSQL database...')
            # self.conn = psycopg2.connect(database=DBNAME, user=DBUSER, password=DBPASS, host=DBHOST, port=DBPORT)
            cur = self.conn.cursor()
            LOGGER.debug('PostgreSQL database version:')
            cur.execute('SELECT version()')
            db_version = cur.fetchone()
            LOGGER.debug(db_version)
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            LOGGER.error(error)
        finally:
            if self.conn is not None:
                self.conn.close()
                LOGGER.debug('Database connection closed.')

    def startEvent(self):

        self.resize(self.settings.value("size", QSize(270, 225)))
        LOGGER.debug('%s dosyasından size bilgisi okundu: %s' % (self.settings.fileName(), self.settings.value('size')))

        self.move(self.settings.value("pos", QPoint(50, 50)))
        LOGGER.debug('%s dosyasından pos bilgisi okundu: %s' % (self.settings.fileName(), self.settings.value('pos')))

    def closeEvent(self, event):

        self.settings.setValue("size", self.size())
        LOGGER.debug('%s dosyasına size bilgisi yazıldı: %s' % (self.settings.fileName(), self.size()))

        self.settings.setValue("pos", self.pos())
        LOGGER.debug('%s dosyasına pos bilgisi yazıldı: %s' % (self.settings.fileName(), self.pos()))

        event.accept()
        LOGGER.info('---------------------------------------------------------------------------------------\n')
        sys.exit()
