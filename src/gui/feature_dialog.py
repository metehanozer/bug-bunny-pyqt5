# -*- coding: utf-8 -*-

from os.path import expanduser

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QDialog, QDialogButtonBox, QBoxLayout, QLineEdit, QVBoxLayout, QGroupBox, QLabel,
                             QPushButton, QGridLayout, QHBoxLayout, QFileDialog)

from src.util.mt import ICON


class FeatureDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.feture_name_edit = None
        self.feature_file_button = None
        self.feture_file_edit = None

        #######################################################################

        layout = QVBoxLayout()
        layout.addWidget(self.init_input_group())
        layout.addWidget(self.init_button_group())
        layout.setContentsMargins(15, 15, 15, 15)
        layout.setSpacing(15)

        self.setLayout(layout)
        self.setLayoutDirection(Qt.LeftToRight)
        self.setAutoFillBackground(False)
        self.setAcceptDrops(True)

        #######################################################################

        self.setWindowTitle("Add Feature...")
        self.setWindowIcon(QIcon(ICON))

    def init_input_group(self):

        desc_label = QLabel('You can add a screenshot or another file related to defect through the screen...')
        desc_label.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)

        empty_label = QLabel()

        feature_desc_label = QLabel('Feature Description:')
        self.feture_name_edit = QLineEdit(self)

        self.feature_file_button = QPushButton("Feature File", self)
        self.feature_file_button.clicked.connect(self.feture_file_button_clicked)
        self.feture_file_edit = QLineEdit(self)

        layout = QGridLayout()
        layout.addWidget(desc_label, 0, 0, 1, 2)
        layout.addWidget(empty_label, 1, 0, 1, 2)
        layout.addWidget(feature_desc_label, 2, 0, 1, 1)
        layout.addWidget(self.feture_name_edit, 2, 1, 1, 1)
        layout.addWidget(self.feature_file_button, 3, 0, 1, 1)
        layout.addWidget(self.feture_file_edit, 3, 1, 1, 1)
        layout.setContentsMargins(15, 15, 15, 15)
        layout.setHorizontalSpacing(30)
        layout.setRowStretch(4, 1)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_button_group(self):

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, self)
        button_box.layout().setDirection(QBoxLayout.RightToLeft)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        layout = QHBoxLayout()
        layout.addStretch(1)
        layout.addWidget(button_box)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def feture_file_button_clicked(self):
        opt = QFileDialog.Options()
        opt |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getOpenFileName(self, "Select File", expanduser("~"), "All Files (*)", options=opt)
        if file_name:
            self.feture_file_edit.setText(file_name)

    def get_feature(self):
        return self.feture_name_edit.text(), self.feture_file_edit.text()
