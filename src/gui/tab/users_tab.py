# -*- coding: utf-8 -*-

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QLineEdit, QLabel, QHBoxLayout, QGroupBox, QTableView, QAbstractItemView,
                             QComboBox, QGridLayout, QPushButton)

from src.util.mt import LOGGER
from src.util.data import get_titles, get_roles
from src.util.db_manager import DbManager
from src.gui.message_box import MessageBox
from src.model.table_model import TableModel


class UsersTab(QWidget):
    def __init__(self, dash):
        super(UsersTab, self).__init__(dash)

        self.dash = dash

        self.message_box = MessageBox()

        self.user_edit = None
        self.title_combo = None
        self.role1_combo = None
        self.role2_combo = None
        self.role3_combo = None
        self.role4_combo = None
        self.role5_combo = None
        self.update_button = None
        self.table = None
        self.model = None
        self.data = None

        #######################################################################

        layout = QGridLayout()
        layout.addWidget(self.init_left_group(), 0, 0, 1, 1)
        layout.addWidget(self.init_right_group(), 0, 1, 1, 1)
        layout.addWidget(self.init_button_group(), 1, 0, 1, 2)

        layout.setContentsMargins(15, 15, 15, 15)
        layout.setHorizontalSpacing(15)
        layout.setVerticalSpacing(15)
        layout.setRowStretch(0, 1)

        self.setLayout(layout)
        self.setLayoutDirection(Qt.LeftToRight)
        self.setAutoFillBackground(False)
        self.setAcceptDrops(False)

        #######################################################################

        self.init_table_data()

        self.init_users_data()

    def init_left_group(self):

        self.table = QTableView(self)

        self.table.setShowGrid(True)

        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)

        vertical_header = self.table.verticalHeader()
        vertical_header.setVisible(True)

        horizontal_header = self.table.horizontalHeader()
        horizontal_header.setDefaultAlignment(Qt.AlignLeft)
        horizontal_header.setStretchLastSection(True)

        self.table.resizeColumnsToContents()

        self.table.setSortingEnabled(True)

        self.table.doubleClicked.connect(self.table_double_clicked)

        layout = QHBoxLayout()
        layout.addWidget(self.table)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_right_group(self):

        user_label = QLabel('User Name:')
        self.user_edit = QLineEdit(self)
        self.user_edit.setEnabled(False)

        title_label = QLabel('User Title:')
        self.title_combo = QComboBox(self)

        role1_label = QLabel('User Role:')
        self.role1_combo = QComboBox(self)

        role2_label = QLabel('User Role:')
        self.role2_combo = QComboBox(self)

        role3_label = QLabel('User Role:')
        self.role3_combo = QComboBox(self)

        role4_label = QLabel('User Role:')
        self.role4_combo = QComboBox(self)

        role5_label = QLabel('User Role:')
        self.role5_combo = QComboBox(self)

        layout = QGridLayout()
        layout.addWidget(user_label, 0, 0, 1, 1)
        layout.addWidget(self.user_edit, 0, 1, 1, 1)
        layout.addWidget(title_label, 1, 0, 1, 1)
        layout.addWidget(self.title_combo, 1, 1, 1, 1)
        layout.addWidget(role1_label, 2, 0, 1, 1)
        layout.addWidget(self.role1_combo, 2, 1, 1, 1)
        layout.addWidget(role2_label, 3, 0, 1, 1)
        layout.addWidget(self.role2_combo, 3, 1, 1, 1)
        layout.addWidget(role3_label, 4, 0, 1, 1)
        layout.addWidget(self.role3_combo, 4, 1, 1, 1)
        layout.addWidget(role4_label, 5, 0, 1, 1)
        layout.addWidget(self.role4_combo, 5, 1, 1, 1)
        layout.addWidget(role5_label, 6, 0, 1, 1)
        layout.addWidget(self.role5_combo, 6, 1, 1, 1)

        layout.setContentsMargins(15, 15, 15, 15)
        layout.setHorizontalSpacing(30)

        layout.setRowStretch(7, 1)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_button_group(self):

        self.update_button = QPushButton('UPDATE', self)
        self.update_button.clicked.connect(self.update_button_clicked)

        layout = QHBoxLayout()
        layout.addStretch(1)
        layout.addWidget(self.update_button)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_table_data(self):

        self.get_table_data()

        headers = ['Id', 'Name', 'Roles', 'Title']
        self.model = TableModel(self.data, headers, self)
        self.table.setModel(self.model)

        self.update_button.setEnabled(False)

    def init_users_data(self):

        title_list = get_titles()
        role_name_list = get_roles()

        self.title_combo.clear()
        self.title_combo.addItems(title_list)

        self.role1_combo.clear()
        self.role1_combo.addItems(role_name_list)

        self.role2_combo.clear()
        self.role2_combo.addItems(role_name_list)

        self.role3_combo.clear()
        self.role3_combo.addItems(role_name_list)

        self.role4_combo.clear()
        self.role4_combo.addItems(role_name_list)

        self.role5_combo.clear()
        self.role5_combo.addItems(role_name_list)

    def get_table_data(self):

        query = """
            SELECT use_id, use_name, use_roles, (SELECT utit_name FROM user_titles WHERE utit_id = users.utit_id) 
            FROM users 
            ORDER BY use_id
        """

        with DbManager("Kullanıcılar listeleniyor...") as cursor:
            try:
                cursor.execute(query)
                self.data = cursor.fetchall()

            except Exception as err:
                LOGGER.error(err)
                self.message_box.setText(str(err))
                self.message_box.exec_()

    def table_double_clicked(self, signal):
        row = signal.row()
        column = signal.column()
        cell_dict = self.model.itemData(signal)
        cell_value = cell_dict.get(0)

        LOGGER.debug("Row: %s Column: %s Value: %s" % (row, column, cell_value))

        #######################################################################

        if column != 0:
            LOGGER.debug("Sıfırıncı kolon dışında bir kolon seçilmiş...")
            return

        #######################################################################

        user_query = "SELECT use_name, use_roles, utit_id FROM users WHERE use_id = %s"
        with DbManager("Tablodan seçilen kullanıcının bilgileri alınıyor...") as cursor:
            try:
                cursor.execute(user_query, (cell_value,))
                user_data = cursor.fetchone()

            except Exception as err:
                LOGGER.error(err)
                self.message_box.setText(str(err))
                self.message_box.exec_()
                return

        #######################################################################

        # SEÇİLEN KULLANICININ ADI GÖSTERİLİYOR
        self.user_edit.setText(user_data[0])

        #######################################################################

        # SEÇİLEN KULLANICININ ÜNVANINI GÖSTERİLİYOR
        self.title_combo.setCurrentIndex(user_data[2])

        #######################################################################

        # SEÇİLEN KULLANICININ ROLLERİ GÖSTERİLİYOR
        role_id_list = [int(x) for x in user_data[1].split(',')]
        self.role1_combo.setCurrentIndex(role_id_list[0])
        self.role2_combo.setCurrentIndex(role_id_list[1])
        self.role3_combo.setCurrentIndex(role_id_list[2])
        self.role4_combo.setCurrentIndex(role_id_list[3])
        self.role5_combo.setCurrentIndex(role_id_list[4])

        #######################################################################

        self.update_button.setEnabled(True)

    def update_button_clicked(self):

        user_name = self.user_edit.text()
        use_roles = "{},{},{},{},{}".format(
            self.role1_combo.currentIndex(),
            self.role2_combo.currentIndex(),
            self.role3_combo.currentIndex(),
            self.role4_combo.currentIndex(),
            self.role5_combo.currentIndex())
        tit_id = self.title_combo.currentIndex()

        #######################################################################

        query = "UPDATE Users SET use_roles = %s, tit_id = %s WHERE use_name = %s RETURNING use_id"
        with DbManager("%s kullanıcısı için bilgiler güncelleniyor..." % user_name) as cursor:
            try:
                cursor.execute(query, (use_roles, tit_id, user_name))
                use_id = cursor.fetchone()[0]
                LOGGER.debug("%s - %s kullanıcı güncellendi" % (use_id, user_name))

                self.init_table_data()
                self.table.selectRow(use_id)

            except Exception as err:
                LOGGER.error(err)
                self.message_box.setText(str(err))
                self.message_box.exec_()
