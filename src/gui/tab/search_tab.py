# -*- coding: utf-8 -*-

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QComboBox, QHBoxLayout, QVBoxLayout, QTableView, QAbstractItemView,
                             QPushButton, QGroupBox)

from src.util.mt import LOGGER
from src.util.db_manager import DbManager
from src.gui.message_box import MessageBox
from src.model.table_model import TableModel


class SearchTab(QWidget):
    def __init__(self, dash):
        super(SearchTab, self).__init__(dash)

        self.dash = dash

        self.message_box = MessageBox()

        self.user_combo = None
        self.status_combo = None
        self.product_combo = None
        self.city_combo = None
        self.severity_combo = None
        self.priority_combo = None
        self.search_button = None
        self.table = None
        self.model = None
        self.data = None

        #######################################################################

        layout = QVBoxLayout()
        layout.addWidget(self.init_filter_group())
        layout.addWidget(self.init_table_group())
        layout.setContentsMargins(15, 15, 15, 15)
        layout.setSpacing(15)

        self.setLayout(layout)
        self.setLayoutDirection(Qt.LeftToRight)
        self.setAutoFillBackground(False)
        self.setAcceptDrops(False)

        #######################################################################

        self.init_filters_data()

    def init_filter_group(self):

        self.user_combo = QComboBox(self)
        self.status_combo = QComboBox(self)
        self.product_combo = QComboBox(self)
        self.city_combo = QComboBox(self)
        self.severity_combo = QComboBox(self)
        self.priority_combo = QComboBox(self)

        self.search_button = QPushButton('SEARCH', self)
        self.search_button.clicked.connect(self.init_table_data)

        layout = QHBoxLayout()
        layout.addWidget(self.user_combo)
        layout.addWidget(self.status_combo)
        layout.addWidget(self.product_combo)
        layout.addWidget(self.city_combo)
        layout.addWidget(self.severity_combo)
        layout.addWidget(self.priority_combo)
        layout.addStretch(1)
        layout.addWidget(self.search_button)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_table_group(self):

        self.table = QTableView(self)

        self.table.setShowGrid(True)

        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)

        vertical_header = self.table.verticalHeader()
        vertical_header.setVisible(True)

        horizontal_header = self.table.horizontalHeader()
        horizontal_header.setDefaultAlignment(Qt.AlignLeft)
        horizontal_header.setStretchLastSection(True)

        self.table.resizeColumnsToContents()

        self.table.setSortingEnabled(True)

        self.table.doubleClicked.connect(self.table_double_clicked)

        layout = QHBoxLayout()
        layout.addWidget(self.table)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_filters_data(self):

        self.user_combo.clear()
        self.user_combo.addItems(self.dash.combo_data[0])

        self.status_combo.clear()
        self.status_combo.addItems(self.dash.combo_data[1])

        self.product_combo.clear()
        self.product_combo.addItems(self.dash.combo_data[2])

        self.city_combo.clear()
        self.city_combo.addItems(self.dash.combo_data[3])

        self.severity_combo.clear()
        self.severity_combo.addItems(self.dash.combo_data[4])

        self.priority_combo.clear()
        self.priority_combo.addItems(self.dash.combo_data[5])

    def init_table_data(self):

        self.get_data()

        headers = ['Defect Code', 'Reporter', 'Status', 'Project', 'Product', 'City', 'Severity', 'Priority',
                   'Insert Date', 'Edit Date', 'Defect Name', 'Defect Description']
        self.model = TableModel(self.data, headers, self)
        self.table.setModel(self.model)

    def get_data(self):

        use_id = self.user_combo.currentIndex()
        ds_id = self.status_combo.currentIndex()
        prd_id = self.product_combo.currentIndex()
        cit_id = self.city_combo.currentIndex()
        dsev_id = self.severity_combo.currentIndex()
        dpri_id = self.priority_combo.currentIndex()

        #######################################################################

        select_query = """
        SELECT 
            def_code, 
            (SELECT use_name FROM users WHERE use_id = defects.use_id), 
            (SELECT dsta_name FROM defect_statuses WHERE ds_id = defects.ds_id), 
            (SELECT prd_name FROM products WHERE prd_id = defects.prd_id), 
            (SELECT cit_name FROM cities WHERE cit_id = defects.cit_id), 
            (SELECT dsev_name FROM defect_severities WHERE dsev_id = defects.dsev_id), 
            (SELECT dpri_name FROM defect_priorities WHERE dpri_id = defects.dpri_id), 
            def_insert_date, 
            def_edit_date, 
            def_name, 
            def_desc 
        FROM Defects
        """

        #######################################################################

        if use_id == 0:
            use_id_where = ""
        else:
            use_id_where = "AND use_id = %s" % use_id

        if ds_id == 0:
            sta_id_where = ""
        else:
            sta_id_where = "AND ds_id = %s" % ds_id

        if prd_id == 0:
            prd_id_where = ""
        else:
            prd_id_where = "AND prd_id = %s" % prd_id

        if cit_id == 0:
            cit_id_where = ""
        else:
            cit_id_where = "AND cit_id = %s" % cit_id

        if dsev_id == 0:
            dsev_id_where = ""
        else:
            dsev_id_where = "AND dsev_id = %s" % dsev_id

        if dpri_id == 0:
            dpri_id_where = ""
        else:
            dpri_id_where = "AND dpri_id = %s" % dpri_id

        where_query = "WHERE 1 = 1 {} {} {} {} {} {}".format(
            use_id_where, sta_id_where, prd_id_where, cit_id_where, dsev_id_where, dpri_id_where)

        #######################################################################

        order_query = "ORDER BY def_id"

        #######################################################################

        full_query = "{} {} {}".format(select_query, where_query, order_query)

        #######################################################################

        with DbManager("Defect arama işlemi yapılıyor...") as cursor:
            try:
                cursor.execute(full_query)
                self.data = cursor.fetchall()

            except Exception as err:
                LOGGER.error(err)
                self.message_box.setText(str(err))
                self.message_box.exec_()

    def table_double_clicked(self, signal):
        row = signal.row()
        column = signal.column()
        cell_dict = self.model.itemData(signal)
        cell_value = cell_dict.get(0)

        LOGGER.debug("Row: %s Column: %s Value: %s" % (row, column, cell_value))
        # index = signal.sibling(row, 0)
        # index_dict = self.model.itemData(index)
        # index_value = index_dict.get(0)
        # LOGGER.debug("Column 1 contents: %s" % index_value)

        #######################################################################

        if column != 0:
            LOGGER.debug("Sıfırıncı kolon dışında bir kolon seçilmiş...")
            return

        #######################################################################

        self.dash.addEditDefectTab.edit_defect(cell_value)
        self.dash.tabwidget.setCurrentWidget(self.dash.addEditDefectTab)

    def clear_data(self):

        self.user_combo.setCurrentIndex(0)
        self.status_combo.setCurrentIndex(0)
        self.product_combo.setCurrentIndex(0)
        self.city_combo.setCurrentIndex(0)
        self.severity_combo.setCurrentIndex(0)
        self.priority_combo.setCurrentIndex(0)

        # self.model.clear()
