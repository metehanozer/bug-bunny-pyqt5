# -*- coding: utf-8 -*-

import os
import psycopg2
from threading import Thread
from datetime import datetime

from PyQt5.QtCore import Qt, QUrl, QSize
from PyQt5.QtWidgets import (QWidget, QGridLayout, QLabel, QComboBox, QLineEdit, QTextEdit, QPushButton, QHBoxLayout,
                             QVBoxLayout, QGroupBox, QMessageBox)

from src.util.mt import LOGGER
from src.util.data import get_def_code
from src.util.db_manager import DbManager
from src.util.ftp_manager import FtpManager
from src.gui.message_box import MessageBox
from src.gui.feature_dialog import FeatureDialog
from src.gui.feature_layout import FeatureLayout


class AddEditDefectTab(QWidget):
    def __init__(self, dash):
        super(AddEditDefectTab, self).__init__(dash)

        self.dash = dash

        self.message_box = MessageBox()

        self.defect_code_edit = None
        self.status_combo = None
        self.product_combo = None
        self.city_combo = None
        self.severity_combo = None
        self.priority_combo = None
        self.name_edit = None
        self.desc_text = None
        self.feature_layout = None
        self.feature_button = None
        self.clear_button = None
        self.save_button = None

        self.def_code = None

        self.feature = -1

        #######################################################################

        layout = QGridLayout()
        layout.addWidget(self.init_left_group(), 0, 0, 1, 1)
        layout.addWidget(self.init_right_group(), 0, 1, 2, 1)
        layout.addWidget(self.init_feature_group(), 1, 0, 1, 1)
        layout.addWidget(self.init_button_group(), 2, 0, 1, 2)

        layout.setContentsMargins(15, 15, 15, 15)
        layout.setHorizontalSpacing(15)
        layout.setVerticalSpacing(15)
        layout.setRowStretch(1, 1)

        self.setLayout(layout)
        self.setLayoutDirection(Qt.LeftToRight)
        self.setAutoFillBackground(False)
        self.setAcceptDrops(False)

        #######################################################################

        self.init_data()

    def init_left_group(self):

        defect_code_label = QLabel('Defect Code:')
        self.defect_code_edit = QLineEdit(self)
        self.defect_code_edit.setEnabled(False)

        status_label = QLabel('Select Status:')
        self.status_combo = QComboBox(self)

        product_label = QLabel('Select Product:')
        self.product_combo = QComboBox(self)

        city_label = QLabel('Select City:')
        self.city_combo = QComboBox(self)

        severity_label = QLabel('Select Severity:')
        self.severity_combo = QComboBox(self)

        priority_label = QLabel('Select Priority:')
        self.priority_combo = QComboBox(self)

        layout = QGridLayout()
        layout.addWidget(defect_code_label, 0, 0, 1, 1)
        layout.addWidget(self.defect_code_edit, 0, 1, 1, 1)
        layout.addWidget(status_label, 1, 0, 1, 1)
        layout.addWidget(self.status_combo, 1, 1, 1, 1)
        layout.addWidget(product_label, 3, 0, 1, 1)
        layout.addWidget(self.product_combo, 3, 1, 1, 1)
        layout.addWidget(city_label, 4, 0, 1, 1)
        layout.addWidget(self.city_combo, 4, 1, 1, 1)
        layout.addWidget(severity_label, 5, 0, 1, 1)
        layout.addWidget(self.severity_combo, 5, 1, 1, 1)
        layout.addWidget(priority_label, 6, 0, 1, 1)
        layout.addWidget(self.priority_combo, 6, 1, 1, 1)
        layout.setContentsMargins(15, 15, 15, 15)
        layout.setHorizontalSpacing(30)
        # layout.setVerticalSpacing(15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_right_group(self):

        self.name_edit = QLineEdit(self, placeholderText="Please insert defect name. Max 50 character...")
        # self.name_edit.textChanged.connect(lambda text: self.defect_code_label.setStyleSheet("QLabel { color: %s}" % ('green' if text else 'red')))

        self.desc_text = QTextEdit(self, placeholderText="Please insert detailed information about defect...")

        layout = QVBoxLayout()
        layout.addWidget(self.name_edit)
        layout.addWidget(self.desc_text)
        # layout.addStretch(1)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_feature_group(self):

        self.feature_layout = QVBoxLayout()

        self.feature_button = QPushButton('New Feature', self)
        self.feature_button.clicked.connect(self.feature_button_clicked)
        feature_button_layout = QHBoxLayout()
        feature_button_layout.addStretch(1)
        feature_button_layout.addWidget(self.feature_button)

        layout = QVBoxLayout()
        layout.addLayout(self.feature_layout)
        layout.addLayout(feature_button_layout)
        layout.addStretch(1)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox("DEFECT FEATURE")
        groupbox.setLayout(layout)

        return groupbox

    def init_button_group(self):

        self.clear_button = QPushButton('CLEAR', self)
        self.clear_button.clicked.connect(self.clear_data)

        self.save_button = QPushButton('SAVE', self)
        self.save_button.clicked.connect(self.save_button_clicked)

        layout = QHBoxLayout()
        layout.addStretch(1)
        layout.addWidget(self.clear_button)
        layout.addWidget(self.save_button)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_data(self):

        self.status_combo.clear()
        self.status_combo.addItems(self.dash.combo_data[1])

        self.product_combo.clear()
        self.product_combo.addItems(self.dash.combo_data[2])

        self.city_combo.clear()
        self.city_combo.addItems(self.dash.combo_data[3])

        self.severity_combo.clear()
        self.severity_combo.addItems(self.dash.combo_data[4])

        self.priority_combo.clear()
        self.priority_combo.addItems(self.dash.combo_data[5])

        self.name_edit.setText("")
        self.desc_text.setText("")

    def edit_defect(self, def_code):

        self.def_code = def_code

        with DbManager("Seçilen defectin bilgileri alınıyor...") as cursor:
            try:
                cursor.execute("SELECT * FROM Defects WHERE def_code = %s", (self.def_code,))
                defect = cursor.fetchone()

            except Exception as err:
                LOGGER.error(err)
                self.message_box.setText(str(err))
                self.message_box.exec_()

        self.defect_code_edit.setText(self.def_code)
        self.status_combo.setCurrentIndex(defect[3])
        self.product_combo.setCurrentIndex(defect[4])
        self.city_combo.setCurrentIndex(defect[5])
        self.severity_combo.setCurrentIndex(defect[6])
        self.priority_combo.setCurrentIndex(defect[7])

        self.name_edit.setText(defect[8])
        self.desc_text.setText(defect[11])

    def default_style(self):
        self.status_combo.setStyleSheet("QComboBox { border: 1px solid #E0E0E0}")
        self.product_combo.setStyleSheet("QComboBox { border: 1px solid #E0E0E0}")
        self.city_combo.setStyleSheet("QComboBox { border: 1px solid #E0E0E0}")
        self.severity_combo.setStyleSheet("QComboBox { border: 1px solid #E0E0E0}")
        self.priority_combo.setStyleSheet("QComboBox { border: 1px solid #E0E0E0}")

        self.name_edit.setStyleSheet("QLineEdit { border: 1px solid #E0E0E0}")
        self.desc_text.setStyleSheet("QTextEdit { border: 1px solid #E0E0E0}")

    def clear_data(self):

        self.default_style()

        self.def_code = None

        self.defect_code_edit.setText("")
        self.status_combo.setCurrentIndex(0)
        self.product_combo.setCurrentIndex(0)
        self.city_combo.setCurrentIndex(0)
        self.severity_combo.setCurrentIndex(0)
        self.priority_combo.setCurrentIndex(0)

        self.name_edit.setText("")
        self.desc_text.setText("")

    def feature_button_clicked(self):

        feature_dialog = FeatureDialog()
        # feature_dialog.setFixedSize(QSize(550, 250))
        if feature_dialog.exec():
            file_name, file_path = feature_dialog.get_feature()

            #######################################################################

            if file_name == '' or file_path == '':
                self.message_box.setText("Feature name or file missing!")
                self.message_box.exec_()
                return

            #######################################################################

            self.feature_layout.addLayout(FeatureLayout(self, file_name, file_path))

    def save_button_clicked(self):

        #######################################################################

        with DbManager("Varsa feature file ekleniyor...") as cursor:

            for i in range(self.feature_layout.count()):
                feature_layout_item = self.feature_layout.itemAt(i)
                if isinstance(feature_layout_item, QHBoxLayout):
                    feature_desc = feature_layout_item.itemAt(0).widget().objectName()
                    feature_path = feature_layout_item.itemAt(1).widget().objectName()
                    print("%s - %s" % (feature_desc, feature_path))

                    #feature_query = """INSERT INTO DefectFeature (def_id, df_order, df_name, df_link_text, df_link) VALUES (%s,%s,%s,%s,%s);"""
                    #cursor.execute(feature_query, (def_id, i, name, link_text, link,))

        #######################################################################

        self.default_style()

        ds_id = self.status_combo.currentIndex()
        prd_id = self.product_combo.currentIndex()
        cit_id = self.city_combo.currentIndex()
        dsev_id = self.severity_combo.currentIndex()
        dpri_id = self.priority_combo.currentIndex()
        def_name = self.name_edit.text()
        edit_date = datetime.now().strftime("%d.%m.%Y %H:%M:%S")
        def_desc = self.desc_text.toPlainText()

        to_exit = False

        if ds_id == 0:
            self.status_combo.setStyleSheet("QComboBox { border: 1px solid red}")
            to_exit = True
        if prd_id == 0:
            self.product_combo.setStyleSheet("QComboBox { border: 1px solid red}")
            to_exit = True
        if cit_id == 0:
            self.city_combo.setStyleSheet("QComboBox { border: 1px solid red}")
            to_exit = True
        if dsev_id == 0:
            self.severity_combo.setStyleSheet("QComboBox { border: 1px solid red}")
            to_exit = True
        if dpri_id == 0:
            self.priority_combo.setStyleSheet("QComboBox { border: 1px solid red}")
            to_exit = True
        if def_name == "":
            self.name_edit.setStyleSheet("QLineEdit { border: 1px solid red}")
            to_exit = True
        if def_desc == "":
            self.desc_text.setStyleSheet("QTextEdit { border: 1px solid red}")
            to_exit = True

        if to_exit:
            return

            #######################################################################

        if self.def_code is None:
            self.def_code = get_def_code(prd_id)
            query = """INSERT INTO Defects (def_code, use_id, ds_id, prd_id, cit_id, dsev_id, dpri_id, def_name, def_insert_date, def_edit_date, def_desc) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING def_id;"""
            with DbManager("Yeni defect ekleniyor...") as cursor:
                try:
                    cursor.execute(query, (self.def_code, self.dash.use_id, ds_id, prd_id, cit_id, dsev_id, dpri_id, def_name, edit_date, edit_date, def_desc,))
                    def_id = cursor.fetchone()[0]

                    self.defect_code_edit.setText(self.def_code)
                    LOGGER.debug("Defect oluşturuldu. def_id: %s def_code: %s" % (def_id, self.def_code))

                except Exception as err:
                    LOGGER.error(err)
                    self.message_box.setText(str(err))
                    self.message_box.exec_()

        else:
            query = """UPDATE Defects SET ds_id = %s, prd_id = %s, cit_id = %s, dsev_id = %s, dpri_id = %s, def_name = %s, def_edit_date = %s, def_desc = %s WHERE def_code = %s RETURNING def_id;"""
            with DbManager("Defect güncelleniyor...") as cursor:
                try:
                    cursor.execute(query, (ds_id, prd_id, cit_id, dsev_id, dpri_id, def_name, edit_date, def_desc, self.def_code,))
                    def_id = cursor.fetchone()[0]

                    LOGGER.debug("Defect güncellendi. def_id: %s def_code: %s" % (def_id, self.def_code))

                except Exception as err:
                    LOGGER.error(err)
                    self.message_box.setText(str(err))
                    self.message_box.exec_()

    def feature_file_to_ftp_thread(self):
        thread = Thread(target=self.feature_file_to_ftp)
        thread.setDaemon(True)
        thread.start()

    def feature_file_to_ftp(self):
        with FtpManager("%s dosyası ftp sunucuya yükleniyor..." % self.path) as conn:
            with open(self.path, "rb") as file:
                conn.storbinary("STOR %s" % self.file_name, file)
