# -*- coding: utf-8 -*-

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QComboBox, QLabel, QHBoxLayout, QVBoxLayout, QTableView, QAbstractItemView,
                             QPushButton, QGroupBox)

from src.util.mt import LOGGER
from src.util.db_manager import DbManager
from src.gui.message_box import MessageBox
from src.model.table_model import TableModel


class DashboardTab(QWidget):
    def __init__(self, dash):
        super(DashboardTab, self).__init__(dash)

        self.dash = dash

        self.message_box = MessageBox()

        self.product_combo = None
        self.city_combo = None
        self.refresh_button = None
        self.table = None
        self.model = None
        self.data = None

        #######################################################################

        layout = QVBoxLayout()
        layout.addWidget(self.init_filter_group())
        layout.addWidget(self.init_table_group())
        layout.setContentsMargins(15, 15, 15, 15)
        layout.setSpacing(15)
        # layout.setHorizontalSpacing(15)
        # layout.setVerticalSpacing(15)

        self.setLayout(layout)
        self.setLayoutDirection(Qt.LeftToRight)
        self.setAutoFillBackground(False)
        self.setAcceptDrops(False)

        #######################################################################

        self.init_filters_data()

        # UYGULAMA İLK AÇILDIĞINDA DEFECTLER GÖSTERİLİYOR
        self.init_table_data()

    def init_filter_group(self):

        self.product_combo = QComboBox(self)
        self.city_combo = QComboBox(self)

        self.refresh_button = QPushButton('REFRESH', self)
        self.refresh_button.clicked.connect(self.init_table_data)

        layout = QHBoxLayout()
        layout.addWidget(self.product_combo)
        layout.addWidget(self.city_combo)
        layout.addStretch(1)
        layout.addWidget(self.refresh_button)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_table_group(self):

        self.table = QTableView(self)

        self.table.setShowGrid(True)

        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)

        vertical_header = self.table.verticalHeader()
        vertical_header.setVisible(True)

        horizontal_header = self.table.horizontalHeader()
        horizontal_header.setDefaultAlignment(Qt.AlignLeft)
        horizontal_header.setStretchLastSection(True)

        self.table.resizeColumnsToContents()

        self.table.setSortingEnabled(True)

        self.table.doubleClicked.connect(self.table_double_clicked)

        layout = QHBoxLayout()
        layout.addWidget(self.table)
        layout.setContentsMargins(15, 15, 15, 15)

        groupbox = QGroupBox()
        groupbox.setLayout(layout)

        return groupbox

    def init_filters_data(self):

        self.product_combo.clear()
        self.product_combo.addItems(self.dash.combo_data[2])

        self.city_combo.clear()
        self.city_combo.addItems(self.dash.combo_data[3])

    def init_table_data(self):

        self.get_data()

        headers = ['Defect Code', 'Status', 'Project', 'Product', 'City', 'Severity', 'Priority', 'Defect Name']
        self.model = TableModel(self.data, headers, self)
        self.table.setModel(self.model)

        """nrows = len(self.data)
        for row in range(nrows):
            self.table.setRowHeight(row, 25)"""

    def get_data(self):

        use_id = self.dash.use_id
        prd_id = self.product_combo.currentIndex()
        cit_id = self.city_combo.currentIndex()

        #######################################################################

        select_query = """
        SELECT 
            def_code, 
            (SELECT ds_name FROM DefectStatus WHERE ds_id = Defects.ds_id), 
            (SELECT prd_name FROM Products WHERE prd_id = Defects.prd_id), 
            (SELECT cit_name FROM Cities WHERE cit_id = Defects.cit_id), 
            (SELECT sev_name FROM Severities WHERE sev_id = Defects.sev_id), 
            (SELECT pri_name FROM Priorities WHERE pri_id = Defects.pri_id), 
            def_name 
        FROM Defects
        """

        #######################################################################

        if prd_id == 0:
            prd_id_where = ""
        else:
            prd_id_where = "AND prd_id = %s" % prd_id

        if cit_id == 0:
            cit_id_where = ""
        else:
            cit_id_where = "AND cit_id = %s" % cit_id

        where_query = "WHERE use_id = {} AND ds_id IN (1,5,7) {} {}".format(use_id, prd_id_where, cit_id_where)

        #######################################################################

        order_query = "ORDER BY def_edit_date DESC"
        # LOGGER.debug("Get Defects Query: %s" % full_query)

        #######################################################################

        full_query = "{} {} {}".format(select_query, where_query, order_query)

        with DbManager("Dashboard için defectler listeleniyor...") as cursor:
            try:
                cursor.execute(full_query)
                self.data = cursor.fetchall()

            except Exception as err:
                LOGGER.error(err)
                self.message_box.setText(str(err))
                self.message_box.exec_()

    def table_double_clicked(self, signal):
        row = signal.row()
        column = signal.column()
        cell_dict = self.model.itemData(signal)
        cell_value = cell_dict.get(0)

        LOGGER.debug("Row: %s Column: %s Value: %s" % (row, column, cell_value))

        #######################################################################

        if column != 0:
            LOGGER.debug("Sıfırıncı kolon dışında bir kolon seçilmiş...")
            return

        #######################################################################

        self.dash.addEditDefectTab.edit_defect(cell_value)
        self.dash.tabwidget.setCurrentWidget(self.dash.addEditDefectTab)
