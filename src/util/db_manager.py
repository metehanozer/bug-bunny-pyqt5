# -*- coding: utf-8 -*-

import psycopg2

from src.util.mt import LOGGER, POSTGRE_PARAMS


class DbManager(object):
    def __init__(self, message=None):

        self.message = message
        self.conn = None
        self.database = POSTGRE_PARAMS["database"]
        self.user = POSTGRE_PARAMS["user"]
        self.password = POSTGRE_PARAMS["password"]
        self.host = POSTGRE_PARAMS["host_internal"]
        self.port = POSTGRE_PARAMS["port"]

    def __enter__(self):
        LOGGER.debug(self.message)

        try:
            self.conn = psycopg2.connect(database=self.database,
                                         user=self.user,
                                         password=self.password,
                                         host=self.host,
                                         port=self.port)
            return self.conn.cursor()

        except (Exception, psycopg2.DatabaseError) as error:
            LOGGER.error(error)

    def __exit__(self, type, value, traceback):
        self.conn.commit()
        self.conn.close()