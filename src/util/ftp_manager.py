# -*- coding: utf-8 -*-

import socket
from ftplib import FTP

from src.util.mt import TITLE, LOGGER, FTP_PARAMS


class FtpManager(object):
    def __init__(self, message=None):

        self.message = message
        self.conn = None
        self.user = FTP_PARAMS["user"]
        self.password = FTP_PARAMS["password"]
        self.host = FTP_PARAMS["host_internal"]
        self.port = FTP_PARAMS["port"]

    def __enter__(self):
        LOGGER.debug(self.message)

        try:
            self.conn = FTP(self.host)
            self.conn.login(self.user, self.password)
            # self.conn.set_pasv(False)
            self.conn.cwd("%s/data" % TITLE)
            return self.conn

        except (socket.timeout, OSError) as error:
            LOGGER.error(error)

    def __exit__(self, type, value, traceback):
        self.conn.close()