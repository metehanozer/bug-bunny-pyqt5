# -*- coding: utf-8 -*-

from datetime import datetime

from src.util.mt import LOGGER
from src.util.db_manager import DbManager


def create_tables():
    commands1 = (
        """
        CREATE TABLE IF NOT EXISTS DefectStatus (
            ds_id SERIAL PRIMARY KEY, 
            ds_name VARCHAR(50) NOT NULL
        )""",
        """
        CREATE TABLE IF NOT EXISTS Projects (
            prj_id SERIAL PRIMARY KEY, 
            prj_name VARCHAR(50) NOT NULL, 
            prj_desc VARCHAR(255) NOT NULL
        )""",
        """
        CREATE TABLE IF NOT EXISTS Products (
            prd_id SERIAL PRIMARY KEY, 
            prd_code VARCHAR(50) NOT NULL, 
            prd_name VARCHAR(50) NOT NULL, 
            prd_desc VARCHAR(255) NOT NULL
        )""",
        """
        CREATE TABLE IF NOT EXISTS Cities (
            cit_id SERIAL PRIMARY KEY, 
            cit_code INTEGER NOT NULL, 
            cit_name VARCHAR(50) NOT NULL, 
            cit_desc VARCHAR(255)
        )""",
        """
        CREATE TABLE IF NOT EXISTS Severities (
            sev_id SERIAL PRIMARY KEY, 
            sev_name VARCHAR(50) NOT NULL
        )""",
        """
        CREATE TABLE IF NOT EXISTS Priorities (
            pri_id SERIAL PRIMARY KEY, 
            pri_name VARCHAR(50) NOT NULL
        )""",
        """
        CREATE TABLE IF NOT EXISTS Titles (
            tit_id SERIAL PRIMARY KEY, 
            tit_name VARCHAR(50) NOT NULL
        )""",
        """
        CREATE TABLE IF NOT EXISTS Roles (
            rol_id SERIAL PRIMARY KEY, 
            rol_name VARCHAR(50) NOT NULL
        )"""
    )

    with DbManager() as cursor:
        for command1 in commands1:
            LOGGER.debug(command1)
            cursor.execute(command1)

    commands2 = (
        """
        CREATE TABLE IF NOT EXISTS Users (
            use_id SERIAL PRIMARY KEY, 
            use_name VARCHAR(50) NOT NULL, 
            use_roles VARCHAR(255) NOT NULL, 
            tit_id INTEGER NOT NULL, 
                FOREIGN KEY (tit_id) 
                REFERENCES Titles (tit_id) 
                ON UPDATE CASCADE ON DELETE CASCADE 
        )""",
        """
        CREATE TABLE IF NOT EXISTS Defects (
            def_id SERIAL PRIMARY KEY, 
            def_code VARCHAR(50) UNIQUE NOT NULL, 
            use_id INTEGER NOT NULL, 
                FOREIGN KEY (use_id) 
                REFERENCES Users (use_id) 
                ON UPDATE CASCADE ON DELETE CASCADE, 
            ds_id INTEGER NOT NULL, 
                FOREIGN KEY (ds_id) 
                REFERENCES DefectStatus (ds_id) 
                ON UPDATE CASCADE ON DELETE CASCADE, 
            prj_id INTEGER NOT NULL, 
                FOREIGN KEY (prj_id) 
                REFERENCES Projects (prj_id) 
                ON UPDATE CASCADE ON DELETE CASCADE, 
            prd_id INTEGER NOT NULL, 
                FOREIGN KEY (prd_id) 
                REFERENCES Products (prd_id) 
                ON UPDATE CASCADE ON DELETE CASCADE, 
            cit_id INTEGER NOT NULL, 
                FOREIGN KEY (cit_id) 
                REFERENCES Cities (cit_id) 
                ON UPDATE CASCADE ON DELETE CASCADE, 
            sev_id INTEGER NOT NULL, 
                FOREIGN KEY (sev_id) 
                REFERENCES Severities (sev_id) 
                ON UPDATE CASCADE ON DELETE CASCADE, 
            pri_id INTEGER NOT NULL, 
                FOREIGN KEY (pri_id) 
                REFERENCES Priorities (pri_id) 
                ON UPDATE CASCADE ON DELETE CASCADE, 
            def_name VARCHAR(255) NOT NULL, 
            def_insert_date TIMESTAMP NOT NULL, 
            def_edit_date TIMESTAMP NOT NULL, 
            def_desc TEXT NOT NULL
        )"""
    )

    with DbManager() as cursor:
        for command2 in commands2:
            LOGGER.debug(command2)
            cursor.execute(command2)

    commands3 = (
        """
        CREATE TABLE IF NOT EXISTS UserLogs (
            ul_id SERIAL PRIMARY KEY, 
            use_id INTEGER NOT NULL, 
                FOREIGN KEY (use_id) 
                REFERENCES Users (use_id) 
                ON UPDATE CASCADE ON DELETE CASCADE, 
            ul_hostname VARCHAR(50) NOT NULL, 
            ul_access_date TIMESTAMP NOT NULL, 
            ul_version VARCHAR(50) NOT NULL 
        )""",
        """
        CREATE TABLE IF NOT EXISTS DefectFeature (
            def_id INTEGER NOT NULL, 
                FOREIGN KEY (def_id) 
                REFERENCES Defects (def_id) 
                ON UPDATE CASCADE ON DELETE CASCADE, 
            df_order INTEGER NOT NULL, 
            df_desc VARCHAR(50) NOT NULL, 
            df_link_text VARCHAR(50) NOT NULL, 
            df_link VARCHAR(255) NOT NULL
        )"""
    )

    with DbManager() as cursor:
        for command3 in commands3:
            LOGGER.debug(command3)
            cursor.execute(command3)


def insert_status():
    query = "INSERT INTO DefectStatus(ds_id, ds_name) VALUES(%s,%s)"
    projects = [
        (0, "All Status",),
        (1, "New",),
        (2, "Rejected",),
        (3, "Deffered",),
        (4, "Duplicate",),
        (5, "In Progress",),
        (6, "Fixed",),
        (7, "Re-open",),
        (8, "Closed",)
    ]
    with DbManager("insert_status") as cursor:
        cursor.executemany(query, projects)


def insert_projects():
    query = "INSERT INTO Projects(prj_id, prj_name, prj_desc) VALUES(%s,%s,%s)"
    projects = [
        (0, "All Projets", 'All Projets...',),
        (1, "ABYS Bakım", 'Canlıya alınmış elektronik ücret toplama sistemlerinin bakım projesi.',),
        (2, "ABYS Bursa", 'Bursa şehri elektronik ücret toplama sistemi projesi.',),
        (3, "Sofia Validator", 'Sofya şehri validatör uygulaması projesi.',)
    ]
    with DbManager("insert_projects") as cursor:
        cursor.executemany(query, projects)


def insert_products():
    query = "INSERT INTO Products(prd_id, prd_code, prd_name, prd_desc) VALUES(%s,%s,%s,%s)"
    products = [
        (0, "ALL", "All Products", 'All Products...',),
        (1, "PAX", "Pax All In One", 'Pax 9000 serisi poslar için geliştirilen dolum uygulaması.',),
        (2, "VALEN", "Valen Kiosk", 'Valen marka kisoklar için geliştirilen dolum uygulaması.',),
        (3, "TVM500", "TVM 500", 'TVM 500 kiosklar için geliştirilen dolum uygulaması.',),
        (4, "TVM1000", "TVM1000", 'TVM 1000 kiosklar için geliştirilen dolum uygulaması.',),
        (5, "Val5M2", "Val5M2 Validatör", 'Val5M2 validatörler için geliştirilen harcama uygulaması.',),
        (6, "Val5M6", "Val5M6 Validatör", 'Val5M6 validatörler için geliştirilen harcama uygulaması.',)
    ]
    with DbManager("insert_products") as cursor:
        cursor.executemany(query, products)


def insert_cities():
    query = "INSERT INTO Cities(cit_id, cit_code, cit_name) VALUES(%s,%s,%s)"
    cities = [
        (0, 0, 'All Cities',),
        (1, 1, 'Adana',),
        (2, 9, 'Aydın',),
        (3, 10, 'Balıkesir',),
        (4, 16, 'Bursa',),
        (5, 20, 'Denizli',),
        (6, 21, 'Diyarbakır',),
        (7, 26, 'Eskişehir',),
        (8, 31, 'Hatay',),
        (9, 44, 'Malatya',),
        (10, 45, 'Manisa',),
        (11, 54, 'Sakarya',),
        (12, 55, 'Samsun',),
        (13, 61, 'Trabzon',),
        (14, 65, 'Van',),
        (15, 99, 'Test',),
        (16, 159, 'Tekirdağ',),
        (17, 180, 'Rwanda',),
        (18, 181, 'Multan',),
        (19, 183, 'Ivory Coast',),
        (20, 203, 'Afyon',)
    ]
    with DbManager("insert_cities") as cursor:
        cursor.executemany(query, cities)


def insert_severities():
    query = "INSERT INTO Severities(sev_id, sev_name) VALUES(%s,%s)"
    projects = [
        (0, "All Severities",),
        (1, "Ciritical",),
        (2, "Major",),
        (3, "Minor",),
        (4, "Low",),
        (5, "Cosmetic",)
    ]
    with DbManager("insert_severities") as cursor:
        cursor.executemany(query, projects)


def insert_priorities():
    query = "INSERT INTO Priorities(pri_id, pri_name) VALUES(%s,%s)"
    projects = [
        (0, "All Priorities"),
        (1, "Immediate"),
        (2, "High"),
        (3, "Medium"),
        (4, "Low")
    ]
    with DbManager("insert_priorities") as cursor:
        cursor.executemany(query, projects)


def insert_titles():
    query = "INSERT INTO Titles(tit_id, tit_name) VALUES(%s,%s)"
    roles = [
        (0, "Guest",),
        (1, "Admin",),
        (2, "Product Owner",),
        (3, "Scrum Master",),
        (4, "BA Team Manager",),
        (5, "BA Team Leader",),
        (6, "BA Senior Specialist",),
        (7, "BA Specialist",),
        (8, "Developer Team Manager",),
        (9, "Developer Team Leader",),
        (10, "Senior Developer Specialist",),
        (11, "Developer Specialist",),
        (12, "QA Team Manager",),
        (13, "QA Team Leader",),
        (14, "QA Senior Specialist",),
        (15, "QA Specialist",)
    ]
    with DbManager("insert_titles") as cursor:
        cursor.executemany(query, roles)


def insert_roles():
    query = "INSERT INTO Roles(rol_id, rol_name) VALUES(%s,%s)"
    roles = [
        (0, "Guest",),
        (1, "Admin",),
        (2, "BA",),
        (3, "Developer",),
        (4, "QA",)
    ]
    with DbManager("insert_roles") as cursor:
        cursor.executemany(query, roles)


def insert_users():
    query = "INSERT INTO Users(use_id, use_name, use_roles, tit_id) VALUES(%s,%s,%s,%s)"
    users = [
        (0, "All Users", "0,0,0,0,0", 0),
        (1, "metehan.ozer", "0,0,0,0,0", 1)
    ]
    with DbManager("insert_users") as cursor:
        cursor.executemany(query, users)


def insert_defects():
    #insert_date = datetime.now().strftime("%d.%m.%Y %H:%M:%S")
    insert_date = datetime.now()
    query = """
        INSERT INTO Defects(def_id, def_code, use_id, ds_id, prj_id, prd_id, cit_id, sev_id, pri_id, def_name, 
            def_insert_date, def_edit_date, def_desc) 
        VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    defects = [
        (1, "D-ABYS-W-1", 1, 1, 1, 1, 1, 2, 2, "Ad 1", insert_date, insert_date, "Hatanın detaylı açıklaması buraya yazısdhsdhsdhlacak 1."),
        (2, "D-ABYS-W-2", 1, 2, 1, 1, 1, 2, 2, "Ad 2", insert_date, insert_date, "Hatanın detaysdhsdhdslı açıklaması buraya yazılacak 2."),
        (3, "D-ABYS-W-3", 1, 1, 1, 1, 1, 2, 2, "Ad 3", insert_date, insert_date, "Hatanın detaylı açıklaması buraya yazılacak 3."),
        (4, "D-PAX-1", 1, 3, 1, 1, 1, 2, 2, "Ad 4", insert_date, insert_date, "Hatanın detaylı açıhxhsdhsdhsdaması buraya yazılacak r."),
        (5, "D-PAX-2", 1, 1, 3, 2, 5, 2, 2, "Ad 5", insert_date, insert_date, "Hatanın detaylı açıklaması shdsdhsdh sburaya yazılacak 6."),
        (6, "D-ABYS-X-1", 1, 2, 1, 1, 1, 2, 2, "Ad 6", insert_date, insert_date, "Hatanın detaysdhsdhsdhsdhsdhlı açıklaması buraya yazılacak 6."),
        (7, "D-ABYS-W-4", 1, 1, 1, 1, 1, 2, 2, "Ad 32", insert_date, insert_date, "Hatanın detaysdhsdhsdhsdhsdhlı açıklaması buraya yazılacak 6."),
        (8, "D-ABYS-W-5", 1, 2, 1, 1, 1, 2, 2, "Ad 55", insert_date, insert_date, "Hatanın detaysdhsdhsdhsdhsdhlı açıklaması buraya yazılacak 6."),
        (9, "D-ABYS-X-2", 1, 4, 1, 1, 2, 3, 3, "Ad 6", insert_date, insert_date, "Hatanın detaysdhsdhsdhsdhsdhlı açıklaması buraya yazılacak 6."),
        (10, "D-PAX-3", 1, 1, 3, 2, 5, 2, 2, "Ad 5", insert_date, insert_date, "Hatanın detaylı açıklaması shdsdhsdh sburaya yazılacak 6."),
        (11, "D-PAX-4", 1, 1, 3, 2, 5, 2, 2, "D-PAX-4", insert_date, insert_date, "Hatanın detaylı açıklaması shdsdhsdh sburaya yazılacak 6."),
        (12, "D-PAX-5", 1, 1, 3, 2, 5, 2, 2, "D-PAX-5", insert_date, insert_date, "Hatanın detaylı açıklaması shdsdhsdh sburaya yazılacak 6."),
        (13, "D-PAX-6", 1, 1, 3, 2, 5, 2, 2, "D-PAX-6", insert_date, insert_date, "Hatanın detaylı açıklaması shdsdhsdh sburaya yazılacak 6."),
        (14, "D-PAX-7", 1, 1, 3, 2, 5, 2, 2, "D-PAX-6", insert_date, insert_date, "Hatanın detaylı açıklaması shdsdhsdh sburaya yazılacak 6."),
        (15, "D-PAX-8", 1, 1, 3, 2, 5, 2, 2, "D-PAX-7", insert_date, insert_date, "Hatanın detaylı açıklaması shdsdhsdh sburaya yazılacak 6."),
        (16, "D-PAX-9", 1, 1, 3, 2, 5, 2, 2, "D-PAX-8", insert_date, insert_date, "Hatanın detaylı açıklaması shdsdhsdh sburaya yazılacak 6."),
        (17, "D-111-1", 1, 1, 3, 3, 5, 2, 2, "D-PAX-8", insert_date, insert_date, "Hatanın detaylı açıklaması shdsdhsdh sburaya yazılacak 6.")
    ]
    with DbManager("insert_defects") as cursor:
        cursor.executemany(query, defects)
