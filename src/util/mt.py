# -*- coding: utf-8 -*-

import os
import logging.handlers

from src.util.config_manager import get_section_dict

#######################################################################################################################
TITLE = "bugBunny"
VERSION = "0.0.3.0"
CONFIG = "res/config.ini"
DESCRIPTION = "Bug Report Tool"
AUTHOR = "Metehan Ozer"
ICON = "res/image/app.ico"
COM = "mOzer"
# APP_HOME = os.path.join(os.environ["APPDATA"], TITLE)
# if not os.path.exists(APP_HOME): os.makedirs(APP_HOME)
# REST_JSON = "%s/testRunner/rest_services.json" % os.getenv('APPDATA')
# REST_JSON = os.path.expandvars("%APPDATA%/testRunner/rest_services.json")
# REST_JSON = os.path.join(APP_HOME, "rest_services.json")
REST_JSON = os.path.join("res", "rest_services.json")

LDAP_PARAMS = get_section_dict(CONFIG, "ldap")
POSTGRE_PARAMS = get_section_dict(CONFIG, "postgresql")
FTP_PARAMS = get_section_dict(CONFIG, "ftp")
#######################################################################################################################

#######################################################################################################################
LOGGER = logging.getLogger(TITLE)
LOGGER.setLevel(logging.DEBUG)

app_log_file = os.path.join("app.log")
file_handler = logging.handlers.RotatingFileHandler(app_log_file, encoding='utf-8', maxBytes=2097152, backupCount=0)
file_handler.setLevel(logging.INFO)
dosyaFormatter = logging.Formatter(
    '[%(levelname)-5s]'
    ' → %(message)s')
file_handler.setFormatter(dosyaFormatter)
LOGGER.addHandler(file_handler)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
konsolFormatter = logging.Formatter(
    '[%(levelname)-5s]'
    # '[%(asctime)s,%(msecs)03d]'
    # '[PID:%(process)s]'
    '[%(threadName)-10s]'
    '[%(module)s.%(funcName)s:%(lineno)d] → %(message)s',
    datefmt='%d.%m.%Y %H:%M:%S')
console_handler.setFormatter(konsolFormatter)
LOGGER.addHandler(console_handler)
#######################################################################################################################


def get_head_tail_from(path):
    return os.path.split(path)


# UYGULAMA İÇİN HOME DİZİNİ. İŞLETİM SİSTEMİNE GÖRE
def get_app_home():

    basepath = os.environ["APPDATA"]

    app_home = os.path.join(os.environ["APPDATA"], TITLE)
    connections_path = os.path.join(app_home, "connections")
    binary_path = os.path.join(app_home, "binary")
    custom_installer_path = os.path.join(app_home, "installer")

    if not os.path.exists(app_home):
        os.makedirs(app_home)

    return app_home
