# -*- coding: utf-8 -*-

import re
import platform
from datetime import datetime

from src.util.mt import LOGGER, VERSION
from src.util.db_manager import DbManager


def get_combo_data():
    with DbManager("Filters data from db...") as cursor:
        cursor.execute("SELECT use_name FROM Users ORDER BY use_id")
        user_list = [r[0] for r in cursor.fetchall()]

        cursor.execute("SELECT ds_name FROM DefectStatus ORDER BY ds_id")
        status_list = [r[0] for r in cursor.fetchall()]

        cursor.execute("SELECT prd_name FROM Products ORDER BY prd_id")
        product_list = [r[0] for r in cursor.fetchall()]

        cursor.execute("SELECT cit_code, cit_name FROM Cities ORDER BY cit_code")
        city_list = ["%s - %s" % (r[0], r[1]) for r in cursor.fetchall()]

        cursor.execute("SELECT sev_name FROM Severities ORDER BY sev_id")
        severity_list = [r[0] for r in cursor.fetchall()]

        cursor.execute("SELECT pri_name FROM Priorities ORDER BY pri_id")
        priority_list = [r[0] for r in cursor.fetchall()]

        return user_list, status_list, product_list, city_list, severity_list, priority_list


def get_roles_from(user):
    with DbManager("%s için use_roles alınıyor..." % user) as cursor:
        query = "SELECT use_roles FROM users WHERE use_name = %s"
        cursor.execute(query, (user,))
        result = cursor.fetchone()

    string_role_list = result[0].split(',')
    roles = []
    for string_role in string_role_list:
        roles.append(int(string_role))

    return roles


def get_use_id_from(user):
    with DbManager("%s için use_id alınıyor..." % user) as cursor:
        query = "SELECT use_id FROM users WHERE use_name = %s"
        cursor.execute(query, (user,))
        use_id = cursor.fetchone()

        if use_id is None:
            query2 = "INSERT INTO users (use_name, use_roles, tit_id) VALUES (%s, %s, %s) RETURNING use_id;"
            cursor.execute(query2, (user, "0,0,0,0,0", 1))
            new_use_id = cursor.fetchone()
            LOGGER.info("%s - %s kullanıcısı oluşturuldu..." % (new_use_id[0], user))
            return new_use_id[0]

        else:
            return use_id[0]


def get_titles():
    with DbManager("Titles from db...") as cursor:
        cursor.execute("SELECT utit_name FROM user_titles ORDER BY utit_id")
        title_list = [r[0] for r in cursor.fetchall()]

        return title_list


def get_roles():
    with DbManager("Roles from db...") as cursor:
        cursor.execute("SELECT urol_name FROM user_roles ORDER BY urol_id")
        role_list = [r[0] for r in cursor.fetchall()]

        return role_list


def add_log(use_id):
    # KULLANICI VE HOSTNAME İÇİN LOG ATILIYOR
    ulog_hostname = platform.node()
    ulog_access_date = datetime.now().strftime("%d.%m.%Y %H:%M:%S")
    select_query = "SELECT ulog_id FROM user_logs WHERE use_id = %s AND ulog_hostname = %s"
    insert_query = "INSERT INTO user_logs (use_id, ulog_hostname, ulog_access_date, ulog_version) VALUES (%s, %s, %s, %s);"
    update_query = "UPDATE user_logs SET ulog_access_date = %s, ulog_version = %s WHERE use_id = %s AND ulog_hostname = %s"

    with DbManager("%s - %s için user_logs giriliyor..." % (use_id, ulog_hostname)) as cursor:
        cursor.execute(select_query, (use_id, ulog_hostname,))
        ulog_id = cursor.fetchone()

        if ulog_id is None:
            cursor.execute(insert_query, (use_id, ulog_hostname, ulog_access_date, VERSION,))
            LOGGER.debug("%s - %s yeni kayıt eklendi." % (use_id, ulog_hostname))

        else:
            cursor.execute(update_query, (ulog_access_date, VERSION, use_id, ulog_hostname,))
            LOGGER.debug("%s - %s kayıt güncellendi." % (use_id, ulog_hostname))


def get_def_code(prd_id):
    with DbManager("İlgli ürün için def_code belirleniyor...") as cursor:
        cursor.execute("SELECT def_code FROM defects WHERE prd_id = %s ORDER BY def_id DESC LIMIT 1", (prd_id,))
        result = cursor.fetchone()

        # ÜRÜN İÇİN İLK DEFECT
        if result is None:
            cursor.execute("SELECT prd_code FROM products WHERE prd_id = %s", (prd_id,))
            prd_code = cursor.fetchone()[0]
            def_code = "D-%s-1" % prd_code

        # ÜRÜN İÇİN 1 ARTIRILMIŞ def_code
        else:
            result_list = result[0].rsplit('-', 1)
            def_code_number = int(result_list[1])
            def_code = "%s-%s" % (result_list[0], def_code_number + 1)

    return def_code

