#!python
# -*- coding: utf-8 -*-

from src.util import init_db_data

init_db_data.create_tables()
init_db_data.insert_status()
init_db_data.insert_projects()
init_db_data.insert_products()
init_db_data.insert_cities()
init_db_data.insert_severities()
init_db_data.insert_priorities()
init_db_data.insert_titles()
init_db_data.insert_roles()
init_db_data.insert_users()
init_db_data.insert_defects()
