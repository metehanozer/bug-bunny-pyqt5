#!/usr/bin/env pipenv-shebang
# -*- coding: utf8 -*-

import sys
import traceback

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMessageBox, QDialog, QApplication

from src.gui.main_window import MainWindow
from src.gui.login import Login
from src.gui.message_box import MessageBox
from src.util.mt import TITLE, VERSION, LOGGER

sys._excepthook = sys.excepthook


# ############################################### PROGRAM İÇİNDE YÖNETİLMEYEN EXCEPTIONLARI YAKALAR
def catch_error(error_type, error_value, tracebackobj):
    sys._excepthook(error_type, error_value, tracebackobj)
    error_list = traceback.format_exception(error_type, error_value, tracebackobj)
    error_sumary = error_list[-1].replace('\n', '')
    error_sumary = "%s -- %s: %s" % (error_sumary, TITLE, VERSION)
    error_message = ""

    for err in error_list:
        error_message += err + '\n'

    error_messagebox = MessageBox()
    error_messagebox.setIcon(QMessageBox.Critical)
    error_messagebox.setWindowTitle('Exception')
    # TODO error_messagebox.setStyleSheet(stl.qmessageboxstyle1)
    error_messagebox.setTextInteractionFlags(Qt.TextSelectableByMouse)
    error_messagebox.setText(error_sumary)
    error_messagebox.setDetailedText(error_message)
    error_messagebox.exec_()

    sys.exit()
###################################################################################################


def main():
    LOGGER.info("--------------------------------------------- %s ---------------------------------------------" % TITLE)

    app = QApplication(sys.argv)
    sys.excepthook = catch_error
    login = Login()

    # if login.exec_() == QDialog.Accepted:
    #     main_window = MainWindow(login.username)
    if True:
        main_window = MainWindow("metehan.ozer")
        main_window.show()
        main_window.raise_()
        app.exec_()
        sys.exit(app)


if __name__ == '__main__':
    main()
